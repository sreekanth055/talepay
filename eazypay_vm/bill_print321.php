<html>
<title></title>
<head> 
<link rel="stylesheet" type="text/css" media="print" href="custom/css/print.css" />
<style>
table {
    border-collapse: collapse;
}
table td
{
	height:15px;
}
@media print{
	.printButtonClass{display:none;}
	.s1{font-size:15px;}
	.s2{font-size:13px;}
	.s3{font-size:12px;}
	.s4{font-size:20px;}
	.s5{font-size:18px;}
	.s6{font-size:16px;}
	.s7{font-size:8px;}
			}
</style></head>
<body onLoad="window.print()">
<?php if(isset($_GET["back"])){?><a href="<?= $_GET["back"]?>"><button style="float:left;" class="printButtonClass">back</button></a><?php }else{?><a href="dashboard.php"><button style="float:left;" class="printButtonClass">back</button></a><?php }?>
<?php 
include("includes/config.php");
if(isset($_GET["billid"]))
{
	$billid=$_GET["billid"];
	$slct=$conn->query("SELECT * FROM vm_billentry WHERE be_billid='$billid'");
	$row=$slct->fetch_assoc();
	$cnt=$conn->query("SELECT COUNT(bi_billitemid) AS cnt FROM vm_billitems WHERE bi_billid='$billid'");
	$row12=$cnt->fetch_assoc();
	$cnt1=$row12["cnt"];
	$r=1;
	$numbill=$cnt1/30;
	$numbill=$numbill+1;
	$ttl=intval($numbill);
}
$profl = $conn->query("SELECT * FROM vm_shopprofile");
$row3 = $profl->fetch_assoc();
?>
<table style="float:left;text-align:left;" width="200" border="0">
<B><CENTER>INVOICE</CENTER></B>

 <tr>
    <th><b><?= $row3['sp_shopname'] ?></b></th>
  </tr>
  <tr>
    <td><?= $row3['sp_shopaddress'] ?><br/> Ph: <?= $row3['sp_mobile'] ?></td>
  </tr>
</table>

 <table style="float:right;text-align:right;" width="200" border="0">
  <tr>
    <th></th>
    <td style=""><span class="s5">TIN: <?=$row3["sp_tin"]?></span></td>
     </tr>
 <tr>
    <th></th>
    <td><span class="s5">CST No: <?=$row3["sp_cst"]?></span></td>
  </tr>
  
</table><br>
 

<table width="100%">
  <tr>
  <hr style="border-top: dotted 2px;margin-top: 42px;" />
   <td><span class="s3"><b>To. M/s :</b> <?php if($row['be_customerid']=='0'){ $csid=0; echo $row['be_customername'];}
												   else{
													   $slctcust=$conn->query("SELECT * FROM vm_customer WHERE cs_customerid='".$row['be_customerid']."'");
														$rowcus=$slctcust->fetch_assoc();
													 echo $rowcus["cs_customername"];
													   $csid=$rowcus["cs_customerid"];
													   }?></span></td>
  </tr>
  <tr><td><span class="s3">Address :<?php echo $row['be_customeraddress']?></span></td>
      <td style="display:none;"><span class="s5">Address :<?php if($row['be_customerid']=='0'){ echo $row['be_customeraddress'];}else{echo $rowcus["cs_address"];} ?></span></td>
  
<td align="justfy"><span class="s5">Invoice No :<?=$row["be_billnumber"]?>  <?php echo $r."/".$ttl?></span></td></tr>
<tr><td></td><td align="justfy"><span class="s5">Invoice Dt : <?=date('d-m-Y ', strtotime($row["be_billdate"]))?></span></td></tr>
<tr><td></td><td align="justfy"><span class="s5">Vehicle Number : <?=$row["be_vehicle_number"]?></span></td></tr>
<!--  <tr><td>
   Ph: <?php if($row['be_customerid']=='0'){ echo $row['be_customermobile'];}else{echo $rowcus["cs_customerphone"];} ?></td>
  </tr>
  <tr>
    <td><?= $rowcus['cs_address'] ?><br> Email:<?= $rowcus['cs_email'] ?></td>
  </tr>
  <tr>
    
  </tr>-->
  </table>
  
<table border="1" width="100%">
<tr>
<th width="50px" style bgcolor="#999999"><span class="s5"> SI No</span></th>

<th style="display:none;" width="15%"  bgcolor="#999999"><span class="s5">PRODUCT CODE</span></th>
<th  bgcolor="#999999"><span class="s5">PRODUCT NAME</span></th>
<th style="display:none;"  width="15%" bgcolor="#999999"><span class="s5">RATE OF TAX</span></th>
<th  width="10%" bgcolor="#999999"><span class="s5">UNIT PRICE</span></th>
<th   width="15%" bgcolor="#999999"><span class="s5">QTY</span></th>
<th style="display:none;"  width="15%" bgcolor="#999999"><span class="s5">NET AMNT</span></th>
<th style="display:none;"  width="15%" bgcolor="#999999"><span class="s5">TAX AMNT</span></th>
<th   width="15%" bgcolor="#999999"><span class="s5"> TOTAL PRICE</span></th>
</tr>
<?php
$i=1;
$j=0;
$slctitm=$conn->query("SELECT * FROM vm_billitems a LEFT JOIN vm_products b ON b.pr_productid=a.bi_productid WHERE a.bi_billid='$billid' LIMIT 27");
$tot=0;
while($rowitm=$slctitm->fetch_assoc())
{
?>
<tr>
<td align="center"><span class="s6"><?=$i?></span></td>
<td style="display:none;" align="center"><span class="s6"><?= $rowitm["pr_productcode"];?></span></td>
<td><span class="s6">
<?php

	echo $rowitm["pr_productname"];
?>
</span></td>
<td style="display:none;" align="center"><span class="s6"><?=$rowitm["bi_vatper"]?></span></td>
<td align="center"><span class="s6"><?=$rowitm["bi_price"]?></span></td>
<td align="center"><span class="s6"><?=$rowitm["bi_quantity"]?></span></td>
<td style="display:none;" align="center"><span class="s6"><?=$rowitm["bi_quantity"]*$rowitm["bi_price"]?></span></td>
<td style="display:none;" align="center"><span class="s6"><?=$rowitm["bi_vatamount"]?></span></td>
<td align="center"><span class="s6"><?=$rowitm["bi_total"]?><?php $tot=$tot+$rowitm["bi_total"];?></span></td>

</tr>
<?php $i++;}
while($i<=27)
{?>
<tr style="border-bottom:0px solid white;"><td colspan="5"><span class="s6">&nbsp;</span></td></tr>
<?php $i++;}
?>
 <tr  style="border-bottom:0px solid white;border-top:1px solid #000;">
  <?php if($r==$ttl){?><td colspan="4"  align="right"><span class="s5">TOTAL</span></td>
 <td align="center"><span class="s6"><?=$row["be_total"]?></span></td><?php }else{?><td style="border-bottom:#000;" align="right"colspan="4">TOTAL</td><td style="border-bottom:#000;"  align="center"><?=$tot ?></td><?php }?>
 </tr>
 <tr style="border-bottom:0px solid white;">
 <?php if($r==$ttl){?> <td colspan="4" align="right"><span class="s5">DISCOUNT %</span></td>
 <td align="center"><span class="s6"><?=$row["be_discount"]?></span></td><?php }else{?><td colspan="6">&nbsp;</td><?php }?>
</tr>

<tr style="border-bottom:0px solid white;">
<?php if($r==$ttl){?> <td colspan="4" align="right"><span class="s5">TAX %</span></td>
 <td align="center"><span class="s6"><?=$row["be_tax"]?></span></td><?php }else{?><td align="right" colspan="6">Continue...&nbsp;</td><?php }?>
</tr>
<tr style="border-bottom:0px solid white;">
<?php if($r==$ttl){?><td colspan="4" align="right"><span class="s5">TAX AMOUNT </span></td>
 <td align="center"><span class="s6"><?=$row["be_taxamt"]?></span></td><?php }else{?><td colspan="6">&nbsp;</td><?php }?>
</tr>
<tr>
 
 <?php if($r==$ttl){?><td colspan="4" align="right"> <span class="s5">FREIGHT</span></td>
 <td align="center"><span class="s6"><?= $row["be_coolie"]?></span></td><?php }else{?><td colspan="6">&nbsp;</td><?php }?>
</tr>
<tr>
<?php if($r==$ttl){?> <td colspan="4" align="right"><span class="s4"><B>GRAND TOTAL</B></span></td>
 <td align="center"><span class="s4"><B><?=$total= $row["be_gtotal"]?></B></span></td><?php }else{?><td colspan="6">&nbsp;</td><?php }?>
</tr>

 <tr style="display:none;">
 
 <?php if($r==$ttl){?> <td colspan="4" align="right"><span class="s5">PAID AMOUNT</span></td>
 <td align="center"><span class="s6"><?= $row["be_paidamount"]?></span></td><?php }else{?><td colspan="6">&nbsp;</td><?php }?>

</tr>

<tr style="display:none;">
 
 <?php if($r==$ttl){?> <td colspan="4" align="right"><span class="s5">BALANCE</span></td>
 <td align="center"><span class="s6"><?=$row["be_balance"]?></span></td><?php }else{?><td colspan="6">&nbsp;</td><?php }?>
</tr>
<tr>
 <?php if($r==$ttl){?> <td colspan="5"><span class="s6">Grand Total(in words) : <?php $amd=convert_number_to_words($total);echo $amd;?></span></td><?php }else{?><td colspan="5">&nbsp;</td><?php }?>
 
</tr> 
</table>
 

<table width="100%" style="font-size: 11px;margin-bottom:0px;">
<tr>
<td>E & OE</td><td style="text-align:right"><BR>FOR SELVAM HANDLOOMS<BR><BR>Authorised Signatory<br>(with Status & Seal)&nbsp;</td>
</tr>
</table>
<?php
$j=0;
$j=$j+27;

while($cnt1>$j)
{
	$r++;	
$slctitm1=$conn->query("SELECT * FROM vm_billitems a LEFT JOIN vm_products b ON b.pr_productid=a.bi_productid WHERE a.bi_billid='$billid' LIMIT ".$j.", 31");
if($slctitm1->num_rows>0)
{
?>

<!--<table style="float:left;text-align:left;" width="200" border="0">
<B><CENTER>INVOICE</CENTER></B>
 <tr>
    <th><b><?= $row3['sp_shopname'] ?></b></th>
  </tr>
  <tr>
    <td><?= $row3['sp_shopaddress'] ?><br/> Ph: <?= $row3['sp_mobile'] ?></td>
  </tr>
</table>-->

 <!--<table style="float:right;text-align:right;" width="200" border="0">
  <tr>
    <th></th>
    <td style=""><span class="s5">TIN: <?=$row3["sp_tin"]?></span></td>
     </tr>
 <tr>
    <th></th>
    <td><span class="s5">CST No: <?=$row3["sp_cst"]?></span></td>
  </tr>
  
</table><br>-->
 

<table width="100%" style="margin-top:50px;">
  <tr>
 
   <td><span class="s3"><b>To. M/s :</b> <?php if($row['be_customerid']=='0'){ $csid=0; echo $row['be_customername'];}
												   else{
													   $slctcust=$conn->query("SELECT * FROM vm_customer WHERE cs_customerid='".$row['be_customerid']."'");
														$rowcus=$slctcust->fetch_assoc();
													 echo $rowcus["cs_customername"];
													   $csid=$rowcus["cs_customerid"];
													   }?> </span> </td>
  </tr>
  <tr><td><span class="s3">Address :<?php echo $row['be_customeraddress']?></span></td>
      <td style="display:none;"><span class="s5">Address :<?php if($row['be_customerid']=='0'){ echo $row['be_customeraddress'];}else{echo $rowcus["cs_address"];} ?></span></td>
  
<td align="justfy"><span class="s5">Invoice No :<?=$row["be_billnumber"]?>  <?php echo $r."/".$ttl?></span></td></tr>
<tr><td></td><td align="justfy"><span class="s5">Invoice Dt : <?=date('d-m-Y ', strtotime($row["be_billdate"]))?></span></td></tr>
<tr><td></td><td align="justfy"><span class="s5">Vehicle Number : <?=$row["be_vehicle_number"]?></span></td></tr>
<!--  <tr><td>
   Ph: <?php if($row['be_customerid']=='0'){ echo $row['be_customermobile'];}else{echo $rowcus["cs_customerphone"];} ?></td>
  </tr>
  <tr>
    <td><?= $rowcus['cs_address'] ?><br> Email:<?= $rowcus['cs_email'] ?></td>
  </tr>
  <tr>
    
  </tr>-->
  </table>
  
<table border="1" width="100%">
<tr>
<th width="50px" style bgcolor="#999999"><span class="s5"> SI No</span></th>

<th style="display:none;" width="15%"  bgcolor="#999999"><span class="s5">PRODUCT CODE</span></th>
<th  bgcolor="#999999"><span class="s5">PRODUCT NAME</span></th>
<th style="display:none;"  width="15%" bgcolor="#999999"><span class="s5">RATE OF TAX</span></th>
<th  width="10%" bgcolor="#999999"><span class="s5">UNIT PRICE</span></th>
<th   width="15%" bgcolor="#999999"><span class="s5">QTY</span></th>
<th style="display:none;"  width="15%" bgcolor="#999999"><span class="s5">NET AMNT</span></th>
<th style="display:none;"  width="15%" bgcolor="#999999"><span class="s5">TAX AMNT</span></th>
<th   width="15%" bgcolor="#999999"><span class="s5"> TOTAL PRICE</span></th>
</tr>
<?php
$i=$j+1;
$z=$j;
$slctitm=$conn->query("SELECT * FROM vm_billitems a LEFT JOIN vm_products b ON b.pr_productid=a.bi_productid WHERE a.bi_billid='$billid' LIMIT ".$z.", 31");
while($rowitm=$slctitm->fetch_assoc())
{
?>
<tr>
<td align="center"><span class="s6"><?=$i?></span></td>
<td style="display:none;" align="center"><span class="s6"><?= $rowitm["pr_productcode"];?></span></td>
<td><span class="s6">
<?php

	echo $rowitm["pr_productname"];
?>
</span></td>
<td style="display:none;" align="center"><span class="s6"><?=$rowitm["bi_vatper"]?></span></td>
<td align="center"><span class="s6"><?=$rowitm["bi_price"]?></span></td>
<td align="center"><span class="s6"><?=$rowitm["bi_quantity"]?></span></td>
<td style="display:none;" align="center"><span class="s6"><?=$rowitm["bi_quantity"]*$rowitm["bi_price"]?></span></td>
<td style="display:none;" align="center"><span class="s6"><?=$rowitm["bi_vatamount"]?></span></td>
<td align="center"><span class="s6"><?=$rowitm["bi_total"]?></span></td>

</tr>
<?php $i++;}
while($i<=$j+31)
{?>
<tr style="border-bottom:0px solid white;"><td colspan="5">&nbsp;</td></tr>
<?php $i++;}
?>
<tr  style="border-bottom:0px solid white;border-top:1px solid #000;">
  <?php if($r==$ttl){?><td colspan="4"  align="right"><span class="s5">TOTAL(Previous)</span></td>
 <td align="center"><span class="s6"><?=$tot?></span></td><?php }else{?><td colspan="6">&nbsp;</td><?php }?>
 </tr>
 <tr  style="border-bottom:0px solid white;border-top:1px solid #000;">
  <?php if($r==$ttl){?><td colspan="4"  align="right"><span class="s5">TOTAL</span></td>
 <td align="center"><span class="s6"><?=$row["be_total"]?></span></td><?php }else{?><td colspan="6">&nbsp;</td><?php }?>
 </tr>
 <tr style="border-bottom:0px solid white;">
   <?php if($r==$ttl){?><td colspan="4" align="right"><span class="s5">DISCOUNT %</span></td>
 <td align="center"><span class="s6"><?=$row["be_discount"]?></span></td><?php }else{?><td colspan="6">&nbsp;</td><?php }?>
</tr>

<tr style="border-bottom:0px solid white;">
 <?php if($r==$ttl){?><td colspan="4" align="right"><span class="s5">TAX %</span></td>
 <td align="center"><span class="s6"><?=$row["be_tax"]?></span></td><?php }else{?><td align="right" colspan="6">Continue...&nbsp;</td><?php }?>
</tr>
<tr style="border-bottom:0px solid white;">
 <?php if($r==$ttl){?><td colspan="4" align="right"><span class="s5">TAX AMOUNT </span></td>
 <td align="center"><span class="s6"><?=$row["be_taxamt"]?></span></td><?php }else{?><td colspan="6">&nbsp;</td><?php }?>
</tr>
<tr>
 
 <?php if($r==$ttl){?><td colspan="4" align="right"> <span class="s5">FREIGHT</span></td>
 <td align="center"><span class="s6"><?= $row["be_coolie"]?></span></td><?php }else{?><td colspan="6">&nbsp;</td><?php }?>
</tr>
<tr>
 <?php if($r==$ttl){?><td colspan="4" align="right"><span class="s4"><B>GRAND TOTAL</B></span></td>
 <td align="center"><span class="s4"><B><?=$total= $row["be_gtotal"]?></B></span></td><?php }else{?><td colspan="6">&nbsp;</td><?php }?>
</tr>

 <tr style="display:none;">
 
 <?php if($r==$ttl){?><td colspan="4" align="right"><span class="s5">PAID AMOUNT</span></td>
 <td align="center"><span class="s6"><?= $row["be_paidamount"]?></span></td><?php }else{?><td colspan="6">&nbsp;</td><?php }?>
</tr>

<tr style="display:none;">
 
 <?php if($r==$ttl){?><td colspan="4" align="right"><span class="s5">BALANCE</span></td>
 <td align="center"><span class="s6"><?=$row["be_balance"]?></span></td><?php }else{?><td colspan="6">&nbsp;</td><?php }?>
</tr>
<tr>
<?php if($r==$ttl){?> <td colspan="5"><span class="s6">Grand Total(in words) : <?php $amd=convert_number_to_words($total);echo $amd;?></span></td><?php }else{?><td colspan="6">&nbsp;</td><?php }?>
 
</tr> 
</table>
 

<table width="100%" style="font-size:11px; margin-bottom:0px;">
<tr>
<td>E & OE</td><td style="text-align:right"><BR>FOR SELVAM HANDLOOMS<BR><BR>Authorised Signatory<br>(with Status & Seal)&nbsp;</td>
</tr>
</table>

<?php
}$j=$j+27;}?>
<script>

</script>
</body>
</html>