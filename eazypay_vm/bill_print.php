<html>
<title></title>
<head> 
<link rel="stylesheet" type="text/css" media="print" href="custom/css/print.css" />
<style>
body{color:#000000;}
table {
    border-collapse: collapse;
}
table td
{
height:15px;
}
.table-curved-head {
border-collapse: separate;
    border: solid #000000 1px;
border-bottom:0px;
    border-radius: 15px 15px 0px 0px;
}
.table-curved-foot {
border-collapse: separate;
    border: solid #000000 1px;
border-top:0px;
    border-radius: 0px 0px 15px 15px;
    
}
/*.table-body th, .table-body td{
borderolid #000000 1px;
}*/
.table-body th{
background: #000000;
color: white;
}
/*@page { size: 2480px 1754px; margin: 0px;}*/
 /* change the margins as you want them to be. */
/*#footer{bottom:0;position:absolute; padding-right:5px;}*/

@media print{
  .btn {display:none;}
.printButtonClass{display:none;}
.table{width:100%;}
.table th{font-size:10px;}
}
@page {
    /*size: 595px 421px;*/
    margin: 5px 10px 5px 5px; }  
    
table{font-size:16px;color:#000000;
}

.table-body th{
background: #000000;
color: white;}
/*#footer{bottom:5px;position: absolute; padding-right:5px;}
body {
    position: relative;
}
@page:last {
    @bottom-center {
        content: "…";
    }*/
body {font-family:verdana;}
}

}
.s1{font-size:10px;}
	.s2{font-size:13px;}
	.s3{font-size:14px;}
	.s4{font-size:13px;}
	.s5{font-size:18px;}
	.s6{font-size:11px;}
	.s7{font-size:12px;}
	@media print{
	.printButtonClass{display:none;}


</style>


</head>
<body onLoad="window.print()">
<?php if(isset($_GET["back"])){?><a href="<?= $_GET["back"]?>"><button style="float:left;" class="btn">back</button></a><?php }else{?><a href="dashboard.php"><button style="float:left;" class="printButtonClass">back</button></a><?php }?>
<?php 
session_start();
include("includes/config.php");
if(isset($_GET["billid"]))
{
	$billid=$_GET["billid"];
	$slct=$conn->query("SELECT * FROM vm_billentry WHERE be_billid='$billid'");
	$row=$slct->fetch_assoc();
}
$profl = $conn->query("SELECT * FROM vm_shopprofile WHERE sp_shopid='".$_SESSION["admin"]."'");
$row3 = $profl->fetch_assoc();
?>

<table style="text-align:center;" width="100%" border="0">
	<tr>
    <td ><b><span style="font-size:17px;"><?=$row3['sp_shopname']?></span></b><br>
 			<span><?=$row3["sp_shopaddress"]?><br>
      <span><?=$row3["sp_phone"]?> &nbsp;<?=$row3["sp_mobile"]?><br>TAX INVOICE
     </td>  
  </tr>
</table>

<table style="text-align:center;" width="100%" border="0"> 
  <tr>
    <td style="text-align:left;"> Bill No:<?=$row["be_billnumber"]?></td> <td align="right"> Time:<?=date('h:i', strtotime($row["be_billdate"]))?></td>
    
  </tr>
  <tr><td style="text-align:left;">Bill Date:<?=date('d-m-Y', strtotime($row["be_billdate"]))?></td></tr>
 
</table><br>

<table style="text-align:center;" width="100%" border="0">

<thead>
<th rowspan="2" width="7%" bgcolor="#999999"> SI No&nbsp;</th>
<th rowspan="2" colspan="2"  style="display:none;" width="100px" bgcolor="#999999"> ITEM CODE</th>

<th rowspan="4"  colspan="4" style="width:75px;" bgcolor="#999999">ITEM </th>
<th style="display:none;"  width="100px" >UNIT</th>
<th rowspan="2" colspan="2" style="width:75px;" bgcolor="#999999"> RATE&nbsp;</th>

<th rowspan="2" colspan="2" style="width:100px;" bgcolor="#999999"> QTY&nbsp;</th>

<th rowspan="2" colspan="2" style="width:100px;" bgcolor="#999999"> GST</th>

<th rowspan="2" colspan="2" style="width:100px;" bgcolor="#999999"> AMT </th>
</thead>
<tbody>
<?php
$i=1;

$slctitm=$conn->query("SELECT * FROM vm_billitems a LEFT JOIN vm_products b ON b.pr_productid=a.bi_productid WHERE a.bi_billid='$billid'");
while($rowitm=$slctitm->fetch_assoc())
{
  $tot_amnt = $rowitm["bi_total"]/$rowitm["bi_quantity"];
?>
<tr>
    <td align="center"> <?=$i?></td>
    <td style="display:none;">
    <?php

    	echo $rowitm["pr_productcode"];
    ?>
    </td>

    <td align="center" colspan="4">
    <?php

    	echo $rowitm["pr_productname"];
    ?>&nbsp;&nbsp;
    </td>
    <td style="display:none;" colspan="2">
    <?php

    	echo $rowitm["bi_unit"];if($rowitm["bi_unit"]=='bag'){echo " ( ".$rowitm["pr_unit"]." kg )";};
    ?>
    </td>
    <td align="center" colspan="2"><?php echo $rowitm["bi_price"]?></td>

    <td align="center" colspan="2"><?php echo $rowitm["bi_quantity"]?></td>

    <td align="center" colspan="2"><?php echo $rowitm["bi_cgst"]+$rowitm["bi_sgst"]?></td>

    <td align="center" colspan="2"><?php echo $rowitm["bi_total"]?></td>

</tr>
<?php $i++;}
	$cnt=$conn->query("SELECT COUNT(bi_billitemid) AS cnts FROM vm_billitems WHERE bi_billid='$billid'");
	$row12=$cnt->fetch_assoc();

?>
 <tr>
  <td ></td>
 <td ></td>

  </tr>
 <!--<tr>
 <!--<td>Items:<?=$row12["cnts"]?></td>
 <td colspan="4" align="right">TOTAL: &nbsp;</td>
 <td ><?=$row["be_total"]?></td>
 </tr>-->

  <!--<tr>
  <td colspan="5" align="right"><span class="s3">DISCOUNT: &nbsp;</td>
 <td align="right"><span class="s3"><?=$row["be_discount"]?></td>
  </tr>
 <tr>
  <td colspan="4" align="right"><span class="s3">GST (include): &nbsp;</td>
<td align="right"><span class="s3"><?php $slctitm=$conn->query("SELECT SUM(bi_cgst_amt) as ttlcgst, SUM(bi_sgst_amt) as ttlsgst,SUM(bi_igst_amt) as ttligst from vm_billitems where bi_billid='$billid' and bi_isactive='0'");
 $rowitm=$slctitm->fetch_assoc();
 $gst=$rowitm["ttlcgst"]+=$rowitm["ttlsgst"]+=$rowitm["ttligst"];
 echo  round($gst)?></td>
  </tr>-->
  <tr >
   <?php if($row["be_discount"]!='0')
{ ?>
 <tr>

<td colspan="12" align="right">DISCOUNT :</td>
 <td><?=$row["be_discount"]?></td>
</tr>
<?php } ?>


  <td colspan="12" align="right">GRANT TOTAL :</td>
  <td align="right"><?php $tottl =$conn->query("SELECT SUM(bi_total) AS tot FROM vm_billitems WHERE `bi_billid` ='$billid'");
  
  $row5=$tottl->fetch_assoc();
  $totl=$row5["tot"];
  $grntotal = $totl - $row['be_discount'];
                                       
   echo round($grntotal)?></td>
 </tr>
 <tr><td></td></tr>
 <tr><td style="border-bottom: 1px #808080 solid;" colspan="12">Grand Total(in words) : <?php $amd=convert_number_to_words($grntotal);echo $amd;?></td></tr>
  <tr>
  <td colspan="5"> Tax Rate </td><td  colspan="5">SGST</td><td colspan="5">CGST</td></tr>
<tr><td colspan="5">GST 5%  </td><td colspan="5"><?php $slctsgst=$conn->query("select sum(bi_sgst_amt) as sgst from vm_billitems where bi_sgst='2.5' and bi_billid='$billid'"); $rowitm=$slctsgst->fetch_assoc();
 $sgst=$rowitm["sgst"];
if($sgst==''){ echo '0';}else{ echo round($sgst,2);} ?></td><td colspan="5"><?php if($sgst==''){ echo '0';}else{ echo round($sgst,2);}  ?></td></tr>

<tr><td colspan="5"> GST 12% </td><td colspan="5"><?php $slctsgst12=$conn->query("select sum(bi_sgst_amt) as sgst from vm_billitems where bi_sgst='6' and bi_billid='$billid'"); $rowitm12=$slctsgst12->fetch_assoc();
 $sgst12=$rowitm12["sgst"];
 if($sgst12==''){ echo '0';}else{ echo round($sgst12,2);}?></td><td colspan="5"><?php if($sgst12==''){ echo '0';}else{ echo round($sgst12,2);}  ?></td></tr>
<tr><td colspan="5">GST 18%  </td><td colspan="5"><?php $slctsgst18=$conn->query("select sum(bi_sgst_amt) as sgst from vm_billitems where bi_sgst='9' and bi_billid='$billid'"); $rowitm18=$slctsgst18->fetch_assoc();
 $sgst18=$rowitm18["sgst"];
 if($sgst18==''){ echo '0';}else{ echo round($sgst18,2);}?></td><td colspan="5"><?php if($sgst18==''){ echo '0';}else{ echo round($sgst18,2);} ?></td></tr>
<tr><td colspan="5"> GST 28% </td><td colspan="5"><?php $slctsgst28=$conn->query("select sum(bi_sgst_amt) as sgst from vm_billitems where bi_sgst='14' and bi_billid='$billid'"); $rowitm28=$slctsgst28->fetch_assoc();
 $sgst28=$rowitm28["sgst"];
 if($sgst28==''){ echo '0';}else{ echo round($sgst28,2);} ?></td><td colspan="5"><?php if($sgst28==''){ echo '0';}else{ echo round($sgst28,2);}  ?></td>
</tr>

 </tbody>
 <!--<tr><td colspan="4"><span class="s4">* No cash return </td></tr>-->

 </table>

<table style="text-align:center;" width="100%" border="0" style="font-size: 12px;">
<tr>


<tr><td></td></tr>
<tr><td style="text-align:left;">GSTIN :<?=$row3["sp_tin"]?></td></tr>

<tr><td colspan="6" style="text-align:right;">Signatory</td></tr>
</table>

<table style="text-align:center;" width="100%" style="font-size: 12px;" border="0">
<tr>
<td style="text-align:center">Thank You....Visit Again..!</td><td style="text-align:right"></td>
</tr>

</table>
<!--<table width="100%">
<tr>
<td bgcolor="#999999" align="center"><?= $row3['sp_shopname'] ?>, <?= $row3['sp_shopaddress'] ?>, Ph:<?= $row3['sp_phone'] ?>, Mob:<?= $row3['sp_mobile'] ?>, EMAIL:<?= $row3['sp_email'] ?></td>
</tr>
</table>-->

<script>

</script>
</body>
</html>