-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 05, 2019 at 12:01 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 5.6.39

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `usebiller_akbar`
--

-- --------------------------------------------------------

--
-- Table structure for table `administrator_account_name`
--

CREATE TABLE `administrator_account_name` (
  `refid` int(11) NOT NULL,
  `acc_name` varchar(30) NOT NULL DEFAULT '',
  `acc_head` enum('bs','tr','pl') NOT NULL DEFAULT 'bs',
  `group_head` enum('asset','liability','debit','credit') NOT NULL DEFAULT 'asset',
  `other_details` text,
  `act_group_head` varchar(50) NOT NULL,
  `opening_balance` bigint(15) DEFAULT '0',
  `opening_balance_type` enum('debit','credit') DEFAULT NULL,
  `closing_balance` bigint(15) DEFAULT '0',
  `closing_balance_type` enum('debit','credit') DEFAULT NULL,
  `status` char(1) DEFAULT NULL,
  `backup` char(1) DEFAULT NULL,
  `acc_updatedtime` date DEFAULT NULL,
  `acnt_branch` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `administrator_daybook`
--

CREATE TABLE `administrator_daybook` (
  `refid` int(10) NOT NULL,
  `ad_branchid` varchar(100) DEFAULT NULL,
  `dayBookDate` date NOT NULL DEFAULT '0000-00-00',
  `debit` varchar(50) NOT NULL DEFAULT '',
  `credit` varchar(50) NOT NULL DEFAULT '',
  `dayBookContra` enum('Y','N') NOT NULL DEFAULT 'Y',
  `dayBookAmount` double NOT NULL DEFAULT '0',
  `description` text NOT NULL,
  `status` char(1) NOT NULL DEFAULT '',
  `backup` char(1) NOT NULL DEFAULT '',
  `billid` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vm_billentry`
--

CREATE TABLE `vm_billentry` (
  `be_billid` int(11) NOT NULL,
  `user_id` varchar(100) NOT NULL,
  `be_billnumber` int(11) DEFAULT NULL,
  `be_customername` varchar(600) DEFAULT NULL,
  `be_customermobile` varchar(20) DEFAULT NULL,
  `be_customer_tin_num` varchar(100) NOT NULL,
  `be_vehicle_number` varchar(10) NOT NULL,
  `be_billdate` datetime DEFAULT NULL,
  `be_total` float DEFAULT NULL,
  `be_gtotal` varchar(100) NOT NULL,
  `be_paidamount` float DEFAULT NULL,
  `be_paymethod` varchar(250) DEFAULT NULL,
  `be_note` longtext,
  `be_updateddate` datetime DEFAULT NULL,
  `be_updatedby` varchar(250) DEFAULT NULL,
  `be_isactive` int(11) DEFAULT '0',
  `be_discount` float DEFAULT NULL,
  `be_mode` varchar(100) NOT NULL,
  `be_paydate` datetime NOT NULL,
  `be_balance` int(11) NOT NULL,
  `be_customerid` int(11) NOT NULL,
  `be_coolie` varchar(100) NOT NULL,
  `be_oldbal` varchar(100) NOT NULL,
  `be_totvat` float NOT NULL,
  `be_debitid` varchar(100) NOT NULL COMMENT 'cash sales',
  `be_creditid` varchar(100) NOT NULL COMMENT 'credit sales',
  `be_statecode` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vm_billentry`
--

INSERT INTO `vm_billentry` (`be_billid`, `user_id`, `be_billnumber`, `be_customername`, `be_customermobile`, `be_customer_tin_num`, `be_vehicle_number`, `be_billdate`, `be_total`, `be_gtotal`, `be_paidamount`, `be_paymethod`, `be_note`, `be_updateddate`, `be_updatedby`, `be_isactive`, `be_discount`, `be_mode`, `be_paydate`, `be_balance`, `be_customerid`, `be_coolie`, `be_oldbal`, `be_totvat`, `be_debitid`, `be_creditid`, `be_statecode`) VALUES
(36, '1', 0, '', '', '', '', '2018-12-29 18:14:00', 23, '23', 23, NULL, NULL, '2018-12-29 18:17:57', NULL, 0, 0, 'sales', '0000-00-00 00:00:00', 0, 0, '0', '0', 0, '', '', ''),
(37, '1', 1, '', '', '', '', '2018-12-29 18:18:00', 150, '150', 150, NULL, NULL, '2018-12-29 18:19:09', NULL, 0, 0, 'sales', '0000-00-00 00:00:00', 0, 0, '0', '0', 0, '', '', ''),
(38, '1', 2, '', '', '', '', '2018-12-31 09:54:00', 150, '150', 150, NULL, NULL, '2018-12-31 09:56:17', NULL, 0, 0, 'sales', '0000-00-00 00:00:00', 0, 0, '0', '0', 0, '', '', ''),
(39, '1', 3, '', '', '', '', '2019-01-04 17:26:00', 588, '588', 588, NULL, NULL, '2019-01-04 17:27:19', NULL, 0, 0, 'sales', '0000-00-00 00:00:00', 0, 0, '0', '0', 0, '', '', ''),
(40, '1', 4, '', '', '', '', '2019-01-04 17:44:00', 158, '158', 158, NULL, NULL, '2019-01-04 17:46:25', NULL, 0, 0, 'sales', '0000-00-00 00:00:00', 0, 0, '0', '0', 0, '', '', ''),
(41, '1', 5, '', '', '', '', '2019-01-04 19:24:00', 158, '158', 158, NULL, NULL, '2019-01-04 19:24:57', NULL, 0, 0, 'sales', '0000-00-00 00:00:00', 0, 0, '0', '0', 0, '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `vm_billitems`
--

CREATE TABLE `vm_billitems` (
  `bi_billitemid` int(11) NOT NULL,
  `user_id` varchar(100) NOT NULL,
  `bi_billid` int(11) DEFAULT NULL,
  `bi_productid` int(11) DEFAULT NULL,
  `bi_price` float DEFAULT NULL,
  `bi_quantity` float DEFAULT NULL,
  `bi_taxamount` varchar(10) NOT NULL,
  `bi_discount` varchar(10) NOT NULL,
  `bi_total` float DEFAULT NULL,
  `bi_updatedon` datetime DEFAULT NULL,
  `bi_isactive` int(11) DEFAULT '0',
  `bi_vatamount` float DEFAULT NULL,
  `bi_vatper` float DEFAULT NULL,
  `be_coolie` varchar(100) NOT NULL,
  `bi_sgst` float DEFAULT NULL,
  `bi_sgst_amt` float DEFAULT NULL,
  `bi_cgst` float DEFAULT NULL,
  `bi_cgst_amt` float DEFAULT NULL,
  `bi_igst` float DEFAULT NULL,
  `bi_igst_amt` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vm_billitems`
--

INSERT INTO `vm_billitems` (`bi_billitemid`, `user_id`, `bi_billid`, `bi_productid`, `bi_price`, `bi_quantity`, `bi_taxamount`, `bi_discount`, `bi_total`, `bi_updatedon`, `bi_isactive`, `bi_vatamount`, `bi_vatper`, `be_coolie`, `bi_sgst`, `bi_sgst_amt`, `bi_cgst`, `bi_cgst_amt`, `bi_igst`, `bi_igst_amt`) VALUES
(1, '1', 39, 1, 560, 1, '560', '', 588, '2019-01-04 17:27:19', 0, NULL, NULL, '', 2.5, 14, 2.5, 14, 0, 0),
(2, '1', 40, 1, 150, 1, '150', '', 158, '2019-01-04 17:46:25', 0, NULL, NULL, '', 2.5, 3.75, 2.5, 3.75, 0, 0),
(3, '1', 41, 1, 150, 1, '150', '', 158, '2019-01-04 19:24:57', 0, NULL, NULL, '', 2.5, 3.75, 2.5, 3.75, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `vm_catogory`
--

CREATE TABLE `vm_catogory` (
  `ca_categoryid` int(11) NOT NULL,
  `user_id` varchar(100) NOT NULL,
  `ca_categoryname` varchar(200) DEFAULT NULL,
  `ca_vat` varchar(200) DEFAULT NULL,
  `ca_isactive` int(11) DEFAULT '0',
  `ca_updatedtime` datetime DEFAULT NULL,
  `ca_note` longtext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vm_catogory`
--

INSERT INTO `vm_catogory` (`ca_categoryid`, `user_id`, `ca_categoryname`, `ca_vat`, `ca_isactive`, `ca_updatedtime`, `ca_note`) VALUES
(1, '1', 'TAX@5%', '5', 0, '2019-01-04 03:13:06', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `vm_customer`
--

CREATE TABLE `vm_customer` (
  `cs_customerid` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `cs_billnumber` int(11) DEFAULT NULL,
  `cs_customername` varchar(600) DEFAULT NULL,
  `cs_customerphone` varchar(100) DEFAULT NULL,
  `cs_address` varchar(1000) DEFAULT NULL,
  `cs_email` varchar(400) DEFAULT NULL,
  `cs_tin_number` varchar(100) DEFAULT NULL,
  `cs_payment` float DEFAULT NULL,
  `cs_discount` float DEFAULT NULL,
  `cs_paid` float DEFAULT NULL,
  `cs_balance` float DEFAULT NULL,
  `cs_paymethod` varchar(250) DEFAULT NULL,
  `cs_note` longtext,
  `cs_updateddate` datetime DEFAULT NULL,
  `cs_updatedby` varchar(250) DEFAULT NULL,
  `cs_isactive` int(11) DEFAULT '0',
  `cs_acntid` varchar(100) NOT NULL,
  `cs_statecode` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vm_customeritemprice`
--

CREATE TABLE `vm_customeritemprice` (
  `cp_id` int(11) NOT NULL,
  `cp_customer_id` varchar(10) DEFAULT NULL,
  `cp_itemid` varchar(10) DEFAULT NULL,
  `cp_itemprice` varchar(100) DEFAULT NULL,
  `cp_isactive` int(11) DEFAULT '0',
  `cp_updateddate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vm_estimation`
--

CREATE TABLE `vm_estimation` (
  `be_billid` int(11) NOT NULL,
  `user_id` varchar(100) NOT NULL,
  `be_billnumber` int(11) DEFAULT NULL,
  `be_customername` varchar(600) DEFAULT NULL,
  `be_customermobile` varchar(20) DEFAULT NULL,
  `be_customer_tin_num` varchar(100) NOT NULL,
  `be_vehicle_number` varchar(10) NOT NULL,
  `be_billdate` datetime DEFAULT NULL,
  `be_total` float DEFAULT NULL,
  `be_gtotal` varchar(100) NOT NULL,
  `be_paidamount` float DEFAULT NULL,
  `be_paymethod` varchar(250) DEFAULT NULL,
  `be_note` longtext,
  `be_updateddate` datetime DEFAULT NULL,
  `be_updatedby` varchar(250) DEFAULT NULL,
  `be_isactive` int(11) DEFAULT '0',
  `be_discount` float DEFAULT NULL,
  `be_mode` varchar(100) NOT NULL,
  `be_paydate` datetime NOT NULL,
  `be_balance` int(11) NOT NULL,
  `be_customerid` int(11) NOT NULL,
  `be_coolie` varchar(100) NOT NULL,
  `be_oldbal` varchar(100) NOT NULL,
  `be_totvat` float NOT NULL,
  `be_debitid` varchar(100) NOT NULL COMMENT 'cash sales',
  `be_creditid` varchar(100) NOT NULL COMMENT 'credit sales',
  `be_statecode` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vm_estimation`
--

INSERT INTO `vm_estimation` (`be_billid`, `user_id`, `be_billnumber`, `be_customername`, `be_customermobile`, `be_customer_tin_num`, `be_vehicle_number`, `be_billdate`, `be_total`, `be_gtotal`, `be_paidamount`, `be_paymethod`, `be_note`, `be_updateddate`, `be_updatedby`, `be_isactive`, `be_discount`, `be_mode`, `be_paydate`, `be_balance`, `be_customerid`, `be_coolie`, `be_oldbal`, `be_totvat`, `be_debitid`, `be_creditid`, `be_statecode`) VALUES
(1, '1', 6, '', '', '', '', '2017-07-05 05:53:00', 200, '200', 200, NULL, NULL, '2017-07-05 05:54:10', NULL, 0, 0, 'sales', '0000-00-00 00:00:00', 0, 10, '0', '', 0, '', '', ' '),
(2, '1', 7, '', '', '', '', '2017-07-05 11:50:00', 550, '550', 550, NULL, NULL, '2017-07-05 11:51:12', NULL, 0, 0, 'sales', '0000-00-00 00:00:00', 0, 0, '0', '', 0, '', '', ' '),
(3, '1', 8, '', '', '', '', '2017-07-05 11:53:00', 2400, '2400', 2400, NULL, NULL, '2017-07-05 11:54:09', NULL, 0, 0, 'sales', '0000-00-00 00:00:00', 0, 0, '0', '', 0, '', '', ' '),
(4, '1', 9, 'das', '', '', '', '2017-07-10 13:30:00', 350, '350', 350, NULL, NULL, '2017-07-10 13:30:20', NULL, 0, 0, 'sales', '0000-00-00 00:00:00', 0, 0, '0', '', 0, '', '', ' '),
(5, '1', 10, 'das', '', '', '', '2017-07-10 13:32:00', 200, '200', 200, NULL, NULL, '2017-07-10 13:32:40', NULL, 0, 0, 'sales', '0000-00-00 00:00:00', 0, 0, '0', '', 0, '', '', ' ');

-- --------------------------------------------------------

--
-- Table structure for table `vm_estimationitems`
--

CREATE TABLE `vm_estimationitems` (
  `bi_billitemid` int(11) NOT NULL,
  `user_id` varchar(100) NOT NULL,
  `bi_billid` int(11) DEFAULT NULL,
  `bi_productid` int(11) DEFAULT NULL,
  `bi_price` float DEFAULT NULL,
  `bi_quantity` float DEFAULT NULL,
  `bi_discount` varchar(10) NOT NULL,
  `bi_total` float DEFAULT NULL,
  `bi_updatedon` datetime DEFAULT NULL,
  `bi_isactive` int(11) DEFAULT '0',
  `bi_vatamount` float DEFAULT NULL,
  `bi_vatper` float DEFAULT NULL,
  `be_coolie` varchar(100) NOT NULL,
  `bi_sgst` float DEFAULT NULL,
  `bi_sgst_amt` float DEFAULT NULL,
  `bi_cgst` float DEFAULT NULL,
  `bi_cgst_amt` float DEFAULT NULL,
  `bi_igst` float DEFAULT NULL,
  `bi_igst_amt` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vm_estimationitems`
--

INSERT INTO `vm_estimationitems` (`bi_billitemid`, `user_id`, `bi_billid`, `bi_productid`, `bi_price`, `bi_quantity`, `bi_discount`, `bi_total`, `bi_updatedon`, `bi_isactive`, `bi_vatamount`, `bi_vatper`, `be_coolie`, `bi_sgst`, `bi_sgst_amt`, `bi_cgst`, `bi_cgst_amt`, `bi_igst`, `bi_igst_amt`) VALUES
(1, '1', 1, 1, 190.48, 1, '', 200, '2017-07-05 05:54:10', 0, NULL, NULL, '', 0, 0, 0, 0, 0, 0),
(2, '1', 2, 1, 190.48, 1, '', 200, '2017-07-05 11:51:12', 0, NULL, NULL, '', 0, 0, 0, 0, 0, 0),
(3, '1', 2, 3, 333.33, 1, '', 350, '2017-07-05 11:51:12', 0, NULL, NULL, '', 0, 0, 0, 0, 0, 0),
(4, '1', 3, 1, 190.48, 12, '0', 2400, '2017-07-05 11:54:09', 0, NULL, NULL, '', 0, 0, 0, 0, 0, 0),
(5, '1', 4, 3, 333.33, 1, '', 350, '2017-07-10 13:30:20', 0, NULL, NULL, '', 0, 0, 0, 0, 0, 0),
(6, '1', 5, 1, 190.48, 1, '', 200, '2017-07-10 13:32:40', 0, NULL, NULL, '', 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `vm_payment`
--

CREATE TABLE `vm_payment` (
  `pa_paymentid` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `pa_customerid` int(11) DEFAULT NULL,
  `pa_billid` int(11) DEFAULT NULL,
  `pa_balance` float DEFAULT NULL,
  `pa_newpayment` float DEFAULT NULL,
  `pa_newbalance` float DEFAULT NULL,
  `pa_isactive` int(11) DEFAULT '0',
  `pa_updatedon` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vm_products`
--

CREATE TABLE `vm_products` (
  `pr_productid` int(11) NOT NULL,
  `user_id` varchar(100) NOT NULL,
  `pr_productcode` varchar(100) DEFAULT NULL,
  `pr_productname` varchar(1000) DEFAULT NULL,
  `pr_hsn` varchar(1000) DEFAULT NULL,
  `pr_purchaseprice` float DEFAULT NULL,
  `pr_saleprice` float DEFAULT NULL,
  `pr_description` longtext,
  `pr_stock` float DEFAULT NULL,
  `pr_isactive` int(11) DEFAULT '0',
  `pr_updateddate` datetime DEFAULT NULL,
  `pr_type` int(11) DEFAULT NULL,
  `pr_unit` varchar(700) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vm_products`
--

INSERT INTO `vm_products` (`pr_productid`, `user_id`, `pr_productcode`, `pr_productname`, `pr_hsn`, `pr_purchaseprice`, `pr_saleprice`, `pr_description`, `pr_stock`, `pr_isactive`, `pr_updateddate`, `pr_type`, `pr_unit`) VALUES
(1, '1', '1001', 'SAREE', '', 0, 0, NULL, 1881, 0, '2019-01-04 03:14:04', 1, 'Piece'),
(2, '1', '1002', 'BLOUSE BIT', '', 0, 0, '', 317, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(3, '1', '1003', 'SHIRTING', '', 0, 0, '', 2524.85, 0, '0000-00-00 00:00:00', 1, 'M'),
(4, '1', '1004', 'SUITING', '', 0, 0, '', 813.85, 0, '0000-00-00 00:00:00', 1, 'M'),
(5, '1', '1005', 'LINING', '', 0, 0, '', 2394, 0, '0000-00-00 00:00:00', 1, 'M'),
(6, '1', '1006', 'BLOUSE MATRL', '', 0, 0, '', 5958.64, 0, '0000-00-00 00:00:00', 1, 'M'),
(7, '1', '1007', 'CURTAIN CLOTH', '', 0, 0, '', 250.65, 0, '0000-00-00 00:00:00', 1, 'M'),
(8, '1', '1008', 'SET MUNDU', '', 0, 0, '', 31, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(9, '1', '1009', 'DHOTHI', '', 0, 0, '', 196, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(10, '1', '1010', 'LUNGI', '', 0, 0, '', 508, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(11, '1', '1011', 'SAREE', '', 0, 0, '', 1884, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(12, '1', '1012', 'BED SHEET', '', 0, 0, '', 135, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(13, '1', '1013', 'SHEETTING', '', 0, 0, '', 243, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(14, '1', '1014', 'MULL', '', 0, 0, '', 1416.5, 0, '0000-00-00 00:00:00', 1, 'M'),
(15, '1', '1015', 'SHIRT BIT', '', 0, 0, '', 97, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(16, '1', '1016', 'PATTU BIT', '', 0, 0, '', 87, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(17, '1', '1017', 'SATIN', '', 0, 0, '', 119, 0, '0000-00-00 00:00:00', 1, 'M'),
(18, '1', '1018', 'PANTIES', '', 0, 0, '', 2088, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(19, '1', '1019', 'BRA', '', 0, 0, '', 1383, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(20, '1', '1020', 'BANIAN', '', 0, 0, '', 182, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(21, '1', '1021', 'BRIEF', '', 0, 0, '', 1709, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(22, '1', '1022', 'SHIRT', '', 0, 0, '', 746, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(23, '1', '1023', 'FROCK', '', 0, 0, '', 918, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(24, '1', '1024', 'MIDDY TOP', '', 0, 0, '', 402, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(25, '1', '1025', 'CHURIDAR', '', 0, 0, '', 210, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(26, '1', '1026', 'LEGGINGS', '', 0, 0, '', 78, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(27, '1', '1027', 'PATTIYALA', '', 0, 0, '', 79, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(28, '1', '1028', 'GIFT BOX', '', 0, 0, '', 5, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(29, '1', '1029', 'BABASUIT', '', 0, 0, '', 790, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(30, '1', '1030', 'NIGHTY', '', 0, 0, '', 447, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(31, '1', '1031', 'CARPET', '', 0, 0, '', 13, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(32, '1', '1032', 'SOMAN DHOTHI', '', 0, 0, '', 18, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(33, '1', '1033', 'PATTU ROLL', '', 0, 0, '', 3, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(34, '1', '1034', 'SIDE BAG', '', 0, 0, '', 20, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(35, '1', '1035', 'PATTU', '', 0, 0, '', 44, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(36, '1', '1036', 'T SHIRT', '', 0, 0, '', 50, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(37, '1', '1037', 'PATTA PATTI', '', 0, 0, '', 84, 0, '0000-00-00 00:00:00', 1, 'M'),
(38, '1', '1038', 'SHEET', '', 0, 0, '', 3, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(39, '1', '1039', 'NAMASS DRESS', '', 0, 0, '', 17, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(40, '1', '1040', 'NIGHT DRESS', '', 0, 0, '', 21, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(41, '1', '1041', '3/4 SET', '', 0, 0, '', 25, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(42, '1', '1042', 'TRUNK PUNCH', '', 0, 0, '', 18, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(43, '1', '1043', 'PANT ', '', 0, 0, '', 498, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(44, '1', '1044', 'TABLESHEET', '', 0, 0, '', 28, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(45, '1', '1045', 'BABY BED', '', 0, 0, '', 20, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(46, '1', '1046', 'MOSQUITO NET', '', 0, 0, '', 82, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(47, '1', '1047', 'SOTTER', '', 0, 0, '', 59, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(48, '1', '1048', 'BERMUDA', '', 0, 0, '', 53, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(49, '1', '1049', 'SLIP', '', 0, 0, '', 9, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(50, '1', '1050', 'SKIRT', '', 0, 0, '', 315, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(51, '1', '1051', 'TWO - PIECE', '', 0, 0, '', 63, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(52, '1', '1052', 'TRACK SUIT', '', 0, 0, '', 31, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(53, '1', '1053', 'BABY SHEET', '', 0, 0, '', 17, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(54, '1', '1054', 'CHURIDAR MATRL', '', 0, 0, '', 401, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(55, '1', '1055', 'THORTH', '', 0, 0, '', 1964, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(56, '1', '1056', 'PILLOW COVER', '', 0, 0, '', 88, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(57, '1', '1057', 'WESTERN', '', 0, 0, '', 182, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(58, '1', '1058', 'PATTU PAVADA', '', 0, 0, '', 27, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(59, '1', '1059', 'KAAVI MUNDE', '', 0, 0, '', 297, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(60, '1', '1060', 'BED COVER', '', 0, 0, '', 23, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(61, '1', '1061', 'POPLINE', '', 0, 0, '', 199, 0, '0000-00-00 00:00:00', 1, 'M'),
(62, '1', '1062', 'SINGLE DHOTHI', '', 0, 0, '', 215, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(63, '1', '1063', 'M DHOTHI', '', 0, 0, '', 7, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(64, '1', '1064', 'BEDDING CLOTH', '', 0, 0, '', 38.1, 0, '0000-00-00 00:00:00', 1, 'M'),
(65, '1', '1065', 'BLACK MUNDE', '', 0, 0, '', 65, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(66, '1', '1066', 'SALWAY', '', 0, 0, '', 46, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(67, '1', '1067', 'GADA', '', 0, 0, '', 680.7, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(68, '1', '1068', 'TOP', '', 0, 0, '', 358, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(69, '1', '1069', 'PATTU PAVADA BIT', '', 0, 0, '', 29, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(70, '1', '1070', 'CHAIR CANVAS', '', 0, 0, '', 13, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(71, '1', '1071', 'KIDS WEAR', '', 0, 0, '', 15, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(72, '1', '1072', 'MOSQUITO BED', '', 0, 0, '', 17, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(73, '1', '1073', 'ERUMUDI', '', 0, 0, '', 5, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(74, '1', '1074', 'SKARF', '', 0, 0, '', 30, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(75, '1', '1075', 'DRAWER', '', 0, 0, '', 17, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(76, '1', '1076', 'FALLS', '', 0, 0, '', 718, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(77, '1', '1077', 'COLOUR MULL', '', 0, 0, '', 318.4, 0, '0000-00-00 00:00:00', 1, 'M'),
(78, '1', '1078', 'BABY MAT', '', 0, 0, '', 10, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(79, '1', '1079', 'MIDDY', '', 0, 0, '', 4, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(80, '1', '1080', 'SHAWL', '', 0, 0, '', 24, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(81, '1', '1081', 'TURKKI', '', 0, 0, '', 7, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(82, '1', '1082', 'MOSQUITO NET BED', '', 0, 0, '', 2, 0, '0000-00-00 00:00:00', 0, 'Piece'),
(83, '1', '1083', 'BLANKET', '', 0, 0, '', 29, 0, '0000-00-00 00:00:00', 0, 'Piece');

-- --------------------------------------------------------

--
-- Table structure for table `vm_purentry`
--

CREATE TABLE `vm_purentry` (
  `pe_billid` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `pe_billnumber` int(11) DEFAULT NULL,
  `pe_customername` varchar(100) DEFAULT NULL,
  `pe_customermobile` int(11) DEFAULT NULL,
  `pe_billdate` datetime DEFAULT NULL,
  `pe_total` float DEFAULT NULL,
  `pe_gtotal` varchar(100) NOT NULL,
  `pe_oldbal` varchar(100) NOT NULL,
  `pe_paidamount` float DEFAULT NULL,
  `pe_paymethod` varchar(200) DEFAULT NULL,
  `pe_note` longtext,
  `pe_updateddate` datetime DEFAULT NULL,
  `pe_updatedby` varchar(250) DEFAULT NULL,
  `pe_isactive` int(11) DEFAULT '0',
  `pe_discount` float DEFAULT NULL,
  `pe_mode` varchar(100) DEFAULT NULL,
  `pe_paydate` datetime DEFAULT NULL,
  `pe_unitprice` int(100) NOT NULL,
  `pe_balance` float NOT NULL,
  `pe_supplierid` varchar(10) NOT NULL,
  `pe_vehicle_number` varchar(100) NOT NULL,
  `pe_invoice_number` varchar(100) NOT NULL,
  `pe_invoice_date` date NOT NULL,
  `pe_statecode` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vm_puritems`
--

CREATE TABLE `vm_puritems` (
  `pi_billitemid` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `pi_billid` int(11) DEFAULT NULL,
  `pi_productid` int(11) DEFAULT NULL,
  `pi_price` float DEFAULT NULL,
  `pi_quantity` float DEFAULT NULL,
  `pi_total` float DEFAULT NULL,
  `pi_updatedon` datetime DEFAULT NULL,
  `pi_isactive` int(11) DEFAULT '0',
  `pi_vatamount` float DEFAULT NULL,
  `pi_vatper` float DEFAULT NULL,
  `pi_unitprice` int(100) NOT NULL,
  `pi_sgst` float DEFAULT NULL,
  `pi_sgstamt` float DEFAULT NULL,
  `pi_cgst` float DEFAULT NULL,
  `pi_cgstamt` float DEFAULT NULL,
  `pi_igst` float DEFAULT NULL,
  `pi_igstamt` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vm_purorder`
--

CREATE TABLE `vm_purorder` (
  `pe_billid` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `pe_billnumber` int(11) DEFAULT NULL,
  `pe_customername` varchar(100) DEFAULT NULL,
  `pe_customermobile` int(11) DEFAULT NULL,
  `pe_billdate` datetime DEFAULT NULL,
  `pe_total` float DEFAULT NULL,
  `pe_gtotal` varchar(100) NOT NULL,
  `pe_oldbal` varchar(100) NOT NULL,
  `pe_paidamount` float DEFAULT NULL,
  `pe_paymethod` varchar(200) DEFAULT NULL,
  `pe_note` longtext,
  `pe_updateddate` datetime DEFAULT NULL,
  `pe_updatedby` varchar(250) DEFAULT NULL,
  `pe_isactive` int(11) DEFAULT '0',
  `pe_discount` float DEFAULT NULL,
  `pe_mode` varchar(100) DEFAULT NULL,
  `pe_paydate` datetime DEFAULT NULL,
  `pe_unitprice` int(100) NOT NULL,
  `pe_balance` float NOT NULL,
  `pe_supplierid` varchar(10) NOT NULL,
  `pe_vehicle_number` varchar(100) NOT NULL,
  `pe_invoice_number` varchar(100) NOT NULL,
  `pe_invoice_date` date NOT NULL,
  `pe_statecode` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vm_purorderitems`
--

CREATE TABLE `vm_purorderitems` (
  `pi_billitemid` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `pi_billid` int(11) DEFAULT NULL,
  `pi_productid` int(11) DEFAULT NULL,
  `pi_price` float DEFAULT NULL,
  `pi_quantity` float DEFAULT NULL,
  `pi_total` float DEFAULT NULL,
  `pi_updatedon` datetime DEFAULT NULL,
  `pi_isactive` int(11) DEFAULT '0',
  `pi_vatamount` float DEFAULT NULL,
  `pi_vatper` float DEFAULT NULL,
  `pi_unitprice` int(100) NOT NULL,
  `pi_sgst` float DEFAULT NULL,
  `pi_sgstamt` float DEFAULT NULL,
  `pi_cgst` float DEFAULT NULL,
  `pi_cgstamt` float DEFAULT NULL,
  `pi_igst` float DEFAULT NULL,
  `pi_igstamt` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vm_purreturnentry`
--

CREATE TABLE `vm_purreturnentry` (
  `pre_billid` int(11) NOT NULL,
  `user_id` varchar(100) NOT NULL,
  `pre_billnumber` int(11) DEFAULT NULL,
  `pre_customername` varchar(600) DEFAULT NULL,
  `pre_customermobile` varchar(20) DEFAULT NULL,
  `pre_customer_tin_num` varchar(100) NOT NULL,
  `pre_vehicle_number` varchar(10) NOT NULL,
  `pre_billdate` datetime DEFAULT NULL,
  `pre_total` float DEFAULT NULL,
  `pre_gtotal` float NOT NULL,
  `pre_paidamount` float DEFAULT NULL,
  `pre_paymethod` varchar(250) DEFAULT NULL,
  `pre_note` longtext,
  `pre_updateddate` datetime DEFAULT NULL,
  `pre_updatedby` varchar(250) DEFAULT NULL,
  `pre_isactive` int(11) DEFAULT '0',
  `pre_discount` float DEFAULT NULL,
  `pre_mode` varchar(100) NOT NULL,
  `pre_paydate` datetime NOT NULL,
  `pre_balance` int(11) NOT NULL,
  `pre_customerid` int(11) NOT NULL,
  `pre_coolie` varchar(100) NOT NULL,
  `pre_oldbal` varchar(100) NOT NULL,
  `pre_totvat` float DEFAULT NULL,
  `pre_invoice_number` varchar(100) NOT NULL,
  `pre_invoice_date` date NOT NULL,
  `pre_rebill` varchar(100) DEFAULT NULL,
  `pre_statecode` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vm_purreturnitem`
--

CREATE TABLE `vm_purreturnitem` (
  `pri_billitemid` int(11) NOT NULL,
  `user_id` varchar(100) NOT NULL,
  `pri_billid` int(11) NOT NULL,
  `pri_returnbillid` int(11) NOT NULL,
  `pri_productid` int(11) NOT NULL,
  `pri_price` varchar(100) NOT NULL,
  `pri_quantity` float NOT NULL,
  `pri_total` float NOT NULL,
  `pri_updatedon` datetime NOT NULL,
  `pri_isactive` int(11) NOT NULL,
  `pri_vatamount` float NOT NULL,
  `pri_vatper` float NOT NULL,
  `pri_coolie` float NOT NULL,
  `pri_sgst` float DEFAULT NULL,
  `pri_sgstamt` float DEFAULT NULL,
  `pri_cgst` float DEFAULT NULL,
  `pr_cgstamt` float DEFAULT NULL,
  `pri_igst` float DEFAULT NULL,
  `pri_igstamt` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vm_salreturnentry`
--

CREATE TABLE `vm_salreturnentry` (
  `sre_billid` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `sre_billnumber` int(11) DEFAULT NULL,
  `sre_customername` varchar(100) DEFAULT NULL,
  `sre_customeraddress` varchar(200) DEFAULT NULL,
  `sre_customermobile` varchar(100) DEFAULT NULL,
  `sre_customer_tin_num` varchar(100) NOT NULL,
  `sre_billdate` datetime DEFAULT NULL,
  `sre_total` float DEFAULT NULL,
  `sre_gtotal` varchar(100) NOT NULL,
  `sre_oldbal` varchar(100) NOT NULL,
  `sre_paidamount` float DEFAULT NULL,
  `sre_paymethod` varchar(200) DEFAULT NULL,
  `sre_note` longtext,
  `sre_updateddate` datetime DEFAULT NULL,
  `sre_updatedby` varchar(250) DEFAULT NULL,
  `sre_isactive` int(11) DEFAULT '0',
  `sre_discount` float DEFAULT NULL,
  `sre_mode` varchar(100) DEFAULT NULL,
  `sre_paydate` datetime DEFAULT NULL,
  `sre_unitprice` int(100) NOT NULL,
  `sre_balance` float NOT NULL,
  `sre_supplierid` int(10) DEFAULT NULL,
  `sre_vehicle_number` varchar(100) NOT NULL,
  `sre_rebill` varchar(100) DEFAULT NULL,
  `sre_statecode` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vm_salreturnitem`
--

CREATE TABLE `vm_salreturnitem` (
  `sri_billitemid` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `sri_billid` int(11) NOT NULL,
  `sri_returnbillid` int(11) NOT NULL,
  `sri_productid` int(11) NOT NULL,
  `sri_price` float NOT NULL,
  `sri_quantity` float NOT NULL,
  `sri_total` float NOT NULL,
  `sri_updatedon` datetime NOT NULL,
  `sri_isactive` int(11) NOT NULL,
  `sri_vatamount` float NOT NULL,
  `sri_vatper` float NOT NULL,
  `sri_unitprice` float NOT NULL,
  `sri_sgst` float DEFAULT NULL,
  `sri_sgstamt` float DEFAULT NULL,
  `sri_cgst` float DEFAULT NULL,
  `sri_cgstamt` float DEFAULT NULL,
  `sri_igst` float DEFAULT NULL,
  `sri_igstamt` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vm_shopprofile`
--

CREATE TABLE `vm_shopprofile` (
  `sp_shopid` int(11) NOT NULL,
  `sp_shopname` varchar(600) DEFAULT NULL,
  `sp_shopaddress` varchar(1000) DEFAULT NULL,
  `sp_phone` varchar(20) DEFAULT NULL,
  `sp_mobile` varchar(20) DEFAULT NULL,
  `sp_email` varchar(350) DEFAULT NULL,
  `sp_logo` varchar(1000) DEFAULT NULL,
  `sp_username` varchar(50) DEFAULT NULL,
  `sp_password` varchar(50) DEFAULT NULL,
  `sp_isactive` int(11) DEFAULT '0',
  `sp_vatreadymades` float DEFAULT NULL,
  `sp_vatmillgoods` float DEFAULT NULL,
  `sp_tin` varchar(100) NOT NULL,
  `sp_cst` varchar(200) NOT NULL,
  `sp_adddate` date NOT NULL,
  `sp_acnttype` int(11) NOT NULL DEFAULT '0' COMMENT '(0)trail(1)payed',
  `sp_trlprd` int(11) NOT NULL,
  `sp_barcode` varchar(10) NOT NULL,
  `sp_stcode` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vm_shopprofile`
--

INSERT INTO `vm_shopprofile` (`sp_shopid`, `sp_shopname`, `sp_shopaddress`, `sp_phone`, `sp_mobile`, `sp_email`, `sp_logo`, `sp_username`, `sp_password`, `sp_isactive`, `sp_vatreadymades`, `sp_vatmillgoods`, `sp_tin`, `sp_cst`, `sp_adddate`, `sp_acnttype`, `sp_trlprd`, `sp_barcode`, `sp_stcode`) VALUES
(1, 'AKBAR TEXTILES', 'Palakkad', '9876543210', '', '', NULL, 'admin', 'admin123', 0, 0, 0, '123456', '', '2019-01-04', 0, 2, 'US-ebiller', 'KL'),
(2, 'uesr1', '', '', '', '', NULL, 'new', '123', 0, NULL, NULL, '', '', '2017-07-07', 0, 10, 'user', 'KL');

-- --------------------------------------------------------

--
-- Table structure for table `vm_supplier`
--

CREATE TABLE `vm_supplier` (
  `rs_supplierid` int(11) NOT NULL,
  `rs_company_name` varchar(100) NOT NULL,
  `rs_name` varchar(50) DEFAULT NULL,
  `rs_phone` varchar(20) DEFAULT NULL,
  `rs_mobile` varchar(20) DEFAULT NULL,
  `rs_address` longtext,
  `rs_email` varchar(100) NOT NULL,
  `rs_balance` varchar(10) NOT NULL,
  `rs_isactive` int(11) DEFAULT '0',
  `rs_tinnum` varchar(100) NOT NULL,
  `user_id` varchar(10) NOT NULL,
  `rs_acntid` varchar(100) NOT NULL,
  `rs_statecode` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vm_transaction`
--

CREATE TABLE `vm_transaction` (
  `tr_id` int(11) NOT NULL,
  `tr_billid` int(11) NOT NULL,
  `tr_particulars` varchar(500) NOT NULL,
  `tr_openingbalance` float NOT NULL,
  `tr_transactionamount` float NOT NULL,
  `tr_closingbalance` float NOT NULL,
  `tr_date` datetime NOT NULL,
  `tr_transactiontype` varchar(500) NOT NULL,
  `tr_isactive` int(11) NOT NULL DEFAULT '0',
  `tr_updateddate` datetime NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vm_transaction`
--

INSERT INTO `vm_transaction` (`tr_id`, `tr_billid`, `tr_particulars`, `tr_openingbalance`, `tr_transactionamount`, `tr_closingbalance`, `tr_date`, `tr_transactiontype`, `tr_isactive`, `tr_updateddate`, `user_id`) VALUES
(69, 36, 'Sales', 0, 23, 23, '2018-12-29 18:14:00', 'income', 0, '0000-00-00 00:00:00', 1),
(70, 37, 'Sales', 23, 150, 173, '2018-12-29 18:18:00', 'income', 0, '0000-00-00 00:00:00', 1),
(71, 38, 'Sales', 173, 150, 323, '2018-12-31 09:54:00', 'income', 0, '0000-00-00 00:00:00', 1),
(72, 39, 'Sales', 323, 588, 911, '2019-01-04 17:26:00', 'income', 0, '0000-00-00 00:00:00', 1),
(73, 40, 'Sales', 911, 158, 1069, '2019-01-04 17:44:00', 'income', 0, '0000-00-00 00:00:00', 1),
(74, 41, 'Sales', 1069, 158, 1227, '2019-01-04 19:24:00', 'income', 0, '0000-00-00 00:00:00', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `administrator_account_name`
--
ALTER TABLE `administrator_account_name`
  ADD PRIMARY KEY (`refid`);

--
-- Indexes for table `administrator_daybook`
--
ALTER TABLE `administrator_daybook`
  ADD PRIMARY KEY (`refid`);

--
-- Indexes for table `vm_billentry`
--
ALTER TABLE `vm_billentry`
  ADD PRIMARY KEY (`be_billid`);

--
-- Indexes for table `vm_billitems`
--
ALTER TABLE `vm_billitems`
  ADD PRIMARY KEY (`bi_billitemid`);

--
-- Indexes for table `vm_catogory`
--
ALTER TABLE `vm_catogory`
  ADD PRIMARY KEY (`ca_categoryid`);

--
-- Indexes for table `vm_customer`
--
ALTER TABLE `vm_customer`
  ADD PRIMARY KEY (`cs_customerid`);

--
-- Indexes for table `vm_customeritemprice`
--
ALTER TABLE `vm_customeritemprice`
  ADD PRIMARY KEY (`cp_id`);

--
-- Indexes for table `vm_estimation`
--
ALTER TABLE `vm_estimation`
  ADD PRIMARY KEY (`be_billid`);

--
-- Indexes for table `vm_estimationitems`
--
ALTER TABLE `vm_estimationitems`
  ADD PRIMARY KEY (`bi_billitemid`);

--
-- Indexes for table `vm_payment`
--
ALTER TABLE `vm_payment`
  ADD PRIMARY KEY (`pa_paymentid`);

--
-- Indexes for table `vm_products`
--
ALTER TABLE `vm_products`
  ADD PRIMARY KEY (`pr_productid`);

--
-- Indexes for table `vm_purentry`
--
ALTER TABLE `vm_purentry`
  ADD PRIMARY KEY (`pe_billid`);

--
-- Indexes for table `vm_puritems`
--
ALTER TABLE `vm_puritems`
  ADD PRIMARY KEY (`pi_billitemid`);

--
-- Indexes for table `vm_purorder`
--
ALTER TABLE `vm_purorder`
  ADD PRIMARY KEY (`pe_billid`);

--
-- Indexes for table `vm_purorderitems`
--
ALTER TABLE `vm_purorderitems`
  ADD PRIMARY KEY (`pi_billitemid`);

--
-- Indexes for table `vm_purreturnentry`
--
ALTER TABLE `vm_purreturnentry`
  ADD PRIMARY KEY (`pre_billid`);

--
-- Indexes for table `vm_purreturnitem`
--
ALTER TABLE `vm_purreturnitem`
  ADD PRIMARY KEY (`pri_billitemid`);

--
-- Indexes for table `vm_salreturnentry`
--
ALTER TABLE `vm_salreturnentry`
  ADD PRIMARY KEY (`sre_billid`);

--
-- Indexes for table `vm_salreturnitem`
--
ALTER TABLE `vm_salreturnitem`
  ADD PRIMARY KEY (`sri_billitemid`);

--
-- Indexes for table `vm_shopprofile`
--
ALTER TABLE `vm_shopprofile`
  ADD PRIMARY KEY (`sp_shopid`);

--
-- Indexes for table `vm_supplier`
--
ALTER TABLE `vm_supplier`
  ADD PRIMARY KEY (`rs_supplierid`);

--
-- Indexes for table `vm_transaction`
--
ALTER TABLE `vm_transaction`
  ADD PRIMARY KEY (`tr_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `administrator_account_name`
--
ALTER TABLE `administrator_account_name`
  MODIFY `refid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `administrator_daybook`
--
ALTER TABLE `administrator_daybook`
  MODIFY `refid` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `vm_billentry`
--
ALTER TABLE `vm_billentry`
  MODIFY `be_billid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `vm_billitems`
--
ALTER TABLE `vm_billitems`
  MODIFY `bi_billitemid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `vm_catogory`
--
ALTER TABLE `vm_catogory`
  MODIFY `ca_categoryid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `vm_customer`
--
ALTER TABLE `vm_customer`
  MODIFY `cs_customerid` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `vm_customeritemprice`
--
ALTER TABLE `vm_customeritemprice`
  MODIFY `cp_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `vm_estimation`
--
ALTER TABLE `vm_estimation`
  MODIFY `be_billid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `vm_estimationitems`
--
ALTER TABLE `vm_estimationitems`
  MODIFY `bi_billitemid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `vm_payment`
--
ALTER TABLE `vm_payment`
  MODIFY `pa_paymentid` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `vm_products`
--
ALTER TABLE `vm_products`
  MODIFY `pr_productid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=84;

--
-- AUTO_INCREMENT for table `vm_purentry`
--
ALTER TABLE `vm_purentry`
  MODIFY `pe_billid` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `vm_puritems`
--
ALTER TABLE `vm_puritems`
  MODIFY `pi_billitemid` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `vm_purorder`
--
ALTER TABLE `vm_purorder`
  MODIFY `pe_billid` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `vm_purorderitems`
--
ALTER TABLE `vm_purorderitems`
  MODIFY `pi_billitemid` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `vm_purreturnentry`
--
ALTER TABLE `vm_purreturnentry`
  MODIFY `pre_billid` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `vm_purreturnitem`
--
ALTER TABLE `vm_purreturnitem`
  MODIFY `pri_billitemid` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `vm_salreturnentry`
--
ALTER TABLE `vm_salreturnentry`
  MODIFY `sre_billid` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `vm_salreturnitem`
--
ALTER TABLE `vm_salreturnitem`
  MODIFY `sri_billitemid` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `vm_shopprofile`
--
ALTER TABLE `vm_shopprofile`
  MODIFY `sp_shopid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `vm_supplier`
--
ALTER TABLE `vm_supplier`
  MODIFY `rs_supplierid` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `vm_transaction`
--
ALTER TABLE `vm_transaction`
  MODIFY `tr_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
