-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 25, 2017 at 06:50 AM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `usebiller_dbselvam`
--

-- --------------------------------------------------------

--
-- Table structure for table `administrator_account_name`
--

CREATE TABLE `administrator_account_name` (
  `refid` int(11) NOT NULL,
  `acc_name` varchar(30) NOT NULL DEFAULT '',
  `acc_head` enum('bs','tr','pl') NOT NULL DEFAULT 'bs',
  `group_head` enum('asset','liability','debit','credit') NOT NULL DEFAULT 'asset',
  `other_details` text,
  `act_group_head` varchar(50) NOT NULL,
  `opening_balance` bigint(15) DEFAULT '0',
  `opening_balance_type` enum('debit','credit') DEFAULT NULL,
  `closing_balance` bigint(15) DEFAULT '0',
  `closing_balance_type` enum('debit','credit') DEFAULT NULL,
  `status` char(1) DEFAULT NULL,
  `backup` char(1) DEFAULT NULL,
  `acc_updatedtime` date DEFAULT NULL,
  `acnt_branch` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `administrator_account_name`
--

INSERT INTO `administrator_account_name` (`refid`, `acc_name`, `acc_head`, `group_head`, `other_details`, `act_group_head`, `opening_balance`, `opening_balance_type`, `closing_balance`, `closing_balance_type`, `status`, `backup`, `acc_updatedtime`, `acnt_branch`) VALUES
(1, 'CASH', 'bs', 'asset', 'CASH', 'CASH', 100000, 'debit', 0, NULL, NULL, NULL, '2016-12-28', '1'),
(2, 'SALES', 'tr', 'credit', 'SALES ACCOUNT', 'SALES', 0, NULL, 0, NULL, NULL, NULL, '2016-12-28', '1'),
(3, 'PURCHASE', 'tr', 'debit', 'PURCHASE ACCOUNT', 'PURCHASE', 0, NULL, 0, NULL, NULL, NULL, '2016-12-28', '1'),
(4, 'CAPITAL', 'bs', 'liability', 'CAPITAL INVESTED', 'CAPITAL', 0, 'debit', 0, NULL, NULL, NULL, '2016-12-28', '1'),
(5, 'OFFICE EXPENSES', 'pl', 'debit', 'OFFICE EXPENSES', 'INDIRECT EXPENSES', 0, NULL, 0, NULL, NULL, NULL, '2016-12-28', '1'),
(6, 'MOBILE EXPENSES', 'pl', 'debit', 'MOBILE EXPENSES', 'INDIRECT EXPENSES', 0, NULL, 0, NULL, NULL, NULL, '2016-12-28', '1'),
(7, 'ACCOUNTING FEES', 'pl', 'debit', 'ACCOUNTING EXPENSES', 'INDIRECT EXPENSES', 0, NULL, 0, NULL, NULL, NULL, '2016-12-28', '1'),
(8, 'AUDIT FEES', 'pl', 'debit', 'AUDIT EXPENSES', 'INDIRECT EXPENSES', 0, NULL, 0, NULL, NULL, NULL, '2016-12-28', '1'),
(9, 'LEGAL FEES', 'pl', 'debit', 'LEGAL EXPENSES', 'INDIRECT EXPENSES', 0, NULL, 0, NULL, NULL, NULL, '2016-12-28', '1'),
(11, 'CUSTOMER 2', 'bs', 'asset', 'CUSTOMER ACCOUNT2', 'SUNDRY DEBTORS', 0, 'debit', 0, NULL, NULL, NULL, '2016-12-28', '1'),
(14, 'VENDOR OR SUPPLIER 1', 'bs', 'liability', 'VENDOR OR SUPPLIER 1', 'SUNDRY CREDITORS', 0, 'debit', 0, NULL, NULL, NULL, '2016-12-28', '1'),
(16, 'BANK ACCOUNT', 'bs', 'asset', 'BANK CURRENT ACCOUNT', 'BANK CURRENT ACCOUNT', 0, NULL, 0, NULL, NULL, NULL, '2016-12-28', '1'),
(17, 'INVESTMENTS', 'bs', 'asset', 'INVESTMENTS ACCOUNT', 'INVESTMENTS', 0, NULL, 0, NULL, NULL, NULL, '2016-12-28', '1'),
(21, 'SUPPLIER1', 'bs', 'liability', 'SUPPLIER1', 'SUNDRY CREDITORS', 0, 'debit', 0, NULL, NULL, NULL, '2017-03-16', '1'),
(22, 'BAR SUPPLIER 2', 'bs', 'liability', 'BAR SUPPLIER 2', 'SUNDRY CREDITORS', 0, 'debit', 0, NULL, NULL, NULL, '2017-03-16', '1'),
(24, 'GUEST GKD', 'bs', 'asset', 'GAYATHRI GUEST', 'SUNDRY DEBTORS', 0, 'debit', 0, NULL, NULL, NULL, '0000-00-00', '1'),
(25, 'GUEST GYA', 'bs', 'asset', 'GAYATHRI KALLADIKKODU GUEST', 'SUNDRY DEBTORS', 0, 'debit', 0, NULL, NULL, NULL, NULL, '1'),
(26, 'TAX VAT', 'bs', 'liability', '', 'DUTIES AND TAXES', 0, 'debit', 0, NULL, NULL, NULL, NULL, '1'),
(28, 'SOORYADAS', 'bs', 'asset', 'SOORYADAS', 'SUNDRY DEBTORS', 0, 'debit', 0, NULL, NULL, NULL, '2017-07-05', '1');

-- --------------------------------------------------------

--
-- Table structure for table `administrator_daybook`
--

CREATE TABLE `administrator_daybook` (
  `refid` int(10) NOT NULL,
  `ad_branchid` varchar(100) DEFAULT NULL,
  `dayBookDate` date NOT NULL DEFAULT '0000-00-00',
  `debit` varchar(50) NOT NULL DEFAULT '',
  `credit` varchar(50) NOT NULL DEFAULT '',
  `dayBookContra` enum('Y','N') NOT NULL DEFAULT 'Y',
  `dayBookAmount` double NOT NULL DEFAULT '0',
  `description` text NOT NULL,
  `status` char(1) NOT NULL DEFAULT '',
  `backup` char(1) NOT NULL DEFAULT '',
  `billid` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `administrator_daybook`
--

INSERT INTO `administrator_daybook` (`refid`, `ad_branchid`, `dayBookDate`, `debit`, `credit`, `dayBookContra`, `dayBookAmount`, `description`, `status`, `backup`, `billid`) VALUES
(1, '1', '2017-06-30', 'CASH', 'SALES', 'Y', 2100, 'SALES', '', '', ''),
(2, '1', '2017-06-30', 'CASH', 'SALES', 'Y', 525, 'SALES', '', '', ''),
(3, '1', '2017-06-30', '', 'CASH', 'Y', 525, 'CREDIT SALES', '', '', ''),
(4, '1', '2017-06-30', 'CASH', 'SALES', 'Y', 1050, 'SALES', '', '', ''),
(5, '1', '2017-06-30', '', 'CASH', 'Y', 1050, 'CREDIT SALES', '', '', ''),
(6, '1', '2017-06-30', 'CASH', 'SALES', 'Y', 1050, 'SALES', '', '', ''),
(7, '1', '2017-06-30', 'SOORYADAS', 'CASH', 'Y', 550, 'CREDIT SALES', '', '', ''),
(8, '1', '2017-06-30', 'PURCHASE', 'CASH', 'Y', 9000, 'PURCHASE', '', '', ''),
(9, '1', '2017-06-30', 'CASH', 'SACHIN SACHIN', 'Y', 4000, 'CREDIT PURCHASE', '', '', ''),
(10, '1', '0000-00-00', 'CASH', 'SALES', 'Y', 0, 'CASH SALES', '', '', ''),
(11, '1', '2017-07-05', 'CASH', 'SALES', 'Y', 200, 'CASH SALES', '', '', ''),
(12, '1', '2017-07-05', 'CASH', 'SALES', 'Y', 200, 'CASH SALES', '', '', ''),
(13, '1', '2017-07-05', 'CASH', 'SALES', 'Y', 534.25, 'CASH SALES', '', '', ''),
(14, '1', '2017-07-05', 'CASH', 'SALES', 'Y', 100, 'test', '', '', ''),
(15, '1', '2017-07-05', 'CASH', 'SOORYADAS', 'Y', 200, 'SALES', '', '', ''),
(16, '1', '2017-07-05', 'CASH', 'SOORYADAS', 'Y', 100, 'SALES', '', '', ''),
(17, '1', '2017-07-05', 'SOORYADAS', 'SALES', 'Y', 100, 'CREDIT SALES', '', '', ''),
(18, '1', '2017-07-06', 'CASH', 'SOORYADAS', 'Y', 200, 'SALES', '', '', ''),
(19, '1', '2017-07-07', 'PURCHASE', 'CASH', 'Y', 100, 'PURCHASE', '', '', ''),
(20, '1', '2017-07-07', 'CASH', 'CUSTOMER 2', 'Y', 200, 'SALES', '', '', ''),
(21, '1', '2017-07-07', 'PURCHASE', 'CASH', 'Y', 100, 'PURCHASE', '', '', ''),
(22, '1', '2017-07-07', 'PURCHASE', 'CASH', 'Y', 100, 'PURCHASE', '', '', ''),
(23, '1', '2017-07-07', 'PURCHASE', 'CASH', 'Y', 100, 'PURCHASE', '', '', ''),
(24, '1', '2017-07-07', 'PURCHASE', 'CASH', 'Y', 100, 'PURCHASE', '', '', ''),
(25, '1', '2017-07-07', 'PURCHASE', 'CASH', 'Y', 100, 'PURCHASE', '', '', ''),
(26, '1', '2017-07-07', 'PURCHASE', 'CASH', 'Y', 100, 'PURCHASE', '', '', ''),
(27, '1', '2017-07-07', 'PURCHASE', 'CASH', 'Y', 100, 'PURCHASE', '', '', ''),
(28, '1', '2017-07-07', 'PURCHASE', 'CASH', 'Y', 100, 'PURCHASE', '', '', ''),
(29, '1', '2017-07-07', 'PURCHASE', 'CASH', 'Y', 100, 'PURCHASE', '', '', ''),
(30, '1', '2017-07-07', 'PURCHASE', 'CASH', 'Y', 300, 'PURCHASE', '', '', ''),
(31, '1', '2017-07-07', 'PURCHASE', 'CASH', 'Y', 100, 'PURCHASE', '', '', ''),
(32, '1', '2017-07-08', 'PURCHASE', 'CASH', 'Y', 100, 'PURCHASE', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `vm_billentry`
--

CREATE TABLE `vm_billentry` (
  `be_billid` int(11) NOT NULL,
  `user_id` varchar(100) NOT NULL,
  `be_billnumber` int(11) DEFAULT NULL,
  `be_customername` varchar(600) DEFAULT NULL,
  `be_customermobile` varchar(20) DEFAULT NULL,
  `be_customer_tin_num` varchar(100) NOT NULL,
  `be_vehicle_number` varchar(10) NOT NULL,
  `be_billdate` datetime DEFAULT NULL,
  `be_total` float DEFAULT NULL,
  `be_gtotal` varchar(100) NOT NULL,
  `be_paidamount` float DEFAULT NULL,
  `be_paymethod` varchar(250) DEFAULT NULL,
  `be_note` longtext,
  `be_updateddate` datetime DEFAULT NULL,
  `be_updatedby` varchar(250) DEFAULT NULL,
  `be_isactive` int(11) DEFAULT '0',
  `be_discount` float DEFAULT NULL,
  `be_mode` varchar(100) NOT NULL,
  `be_paydate` datetime NOT NULL,
  `be_balance` int(11) NOT NULL,
  `be_customerid` int(11) NOT NULL,
  `be_coolie` varchar(100) NOT NULL,
  `be_oldbal` varchar(100) NOT NULL,
  `be_totvat` float NOT NULL,
  `be_debitid` varchar(100) NOT NULL COMMENT 'cash sales',
  `be_creditid` varchar(100) NOT NULL COMMENT 'credit sales',
  `be_statecode` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vm_billentry`
--

INSERT INTO `vm_billentry` (`be_billid`, `user_id`, `be_billnumber`, `be_customername`, `be_customermobile`, `be_customer_tin_num`, `be_vehicle_number`, `be_billdate`, `be_total`, `be_gtotal`, `be_paidamount`, `be_paymethod`, `be_note`, `be_updateddate`, `be_updatedby`, `be_isactive`, `be_discount`, `be_mode`, `be_paydate`, `be_balance`, `be_customerid`, `be_coolie`, `be_oldbal`, `be_totvat`, `be_debitid`, `be_creditid`, `be_statecode`) VALUES
(1, '1', 1, '', '', '123456789', '', '2017-06-30 15:55:00', 2100, '2100', 2100, NULL, NULL, '2017-06-30 15:57:11', NULL, 0, 0, 'sales', '0000-00-00 00:00:00', 0, 1, '0', '', 0, '', '', NULL),
(2, '1', 2, '', '', '123456789', '', '2017-06-30 15:58:00', 525, '525', 0, NULL, NULL, '2017-06-30 15:58:52', NULL, 0, 0, 'sales', '0000-00-00 00:00:00', 525, 1, '0', '', 0, '', '', NULL),
(3, '1', 3, '', '', '', '', '2017-06-30 16:02:00', 1050, '1050', 0, NULL, NULL, '2017-06-30 16:03:03', NULL, 0, 0, 'sales', '0000-00-00 00:00:00', 1050, 3, '0', '', 0, '', '', NULL),
(4, '1', 4, '', '', '', '', '2017-06-30 16:03:00', 1050, '1050', 500, NULL, NULL, '2017-06-30 16:05:15', NULL, 0, 0, 'sales', '0000-00-00 00:00:00', 550, 4, '0', '', 0, '', '', NULL),
(5, '1', 0, '', '', '', '', '1970-01-01 05:30:00', 0, '', 0, NULL, NULL, '2017-07-04 22:26:30', NULL, 0, 0, 'sales', '0000-00-00 00:00:00', 0, 0, '', '', 0, '', '', NULL),
(6, '1', 1, '', '', '123456789', '', '2017-07-05 03:30:00', 200, '200', 200, NULL, NULL, '2017-07-05 03:30:30', NULL, 0, 0, 'sales', '0000-00-00 00:00:00', 0, 1, '0', '', 0, '', '', ' KL'),
(7, '1', 2, '', '', '', '', '2017-07-05 03:31:00', 200, '200', 200, NULL, NULL, '2017-07-05 03:31:14', NULL, 0, 0, 'sales', '0000-00-00 00:00:00', 0, 3, '0', '', 0, '', '', ' GA'),
(8, '1', 3, '', '', '', '', '2017-07-05 03:37:00', 534.25, '534.25', 534.25, NULL, NULL, '2017-07-05 03:37:58', NULL, 0, 15, 'sales', '0000-00-00 00:00:00', 0, 0, '0', '', 0, '', '', ' '),
(9, '1', 4, '', '', '', '', '2017-07-05 05:22:00', 200, '200', 200, NULL, NULL, '2017-07-05 05:23:12', NULL, 0, 0, 'sales', '0000-00-00 00:00:00', 0, 10, '0', '', 0, '', '', ' KL'),
(10, '1', 5, '', '', '', '', '2017-07-05 05:25:00', 200, '200', 100, NULL, NULL, '2017-07-05 05:26:17', NULL, 0, 0, 'sales', '0000-00-00 00:00:00', 100, 10, '0', '', 0, '', '', ' KL'),
(11, '1', 6, '', '', '', '', '2017-07-06 09:59:00', 200, '200', 200, NULL, NULL, '2017-07-06 10:00:05', NULL, 0, 0, 'sales', '0000-00-00 00:00:00', 0, 10, '0', '', 0, '', '', ' KA'),
(12, '1', 7, '', '', '123456789', '', '2017-07-07 14:38:00', 200, '200', 200, NULL, NULL, '2017-07-07 14:38:49', NULL, 0, 0, 'sales', '0000-00-00 00:00:00', 0, 1, '0', '', 0, '', '', ' KL'),
(13, '1', 8, '', '', '123456789', '', '2017-07-07 17:49:00', 200, '925', 925, NULL, NULL, '2017-07-07 17:49:24', NULL, 0, 0, 'sales', '0000-00-00 00:00:00', 0, 1, '0', '725', 0, '', '', NULL),
(14, '1', 9, '', '', '123456789', '', '2017-07-07 17:49:00', 200, '200', 200, NULL, NULL, '2017-07-07 17:49:39', NULL, 0, 0, 'sales', '0000-00-00 00:00:00', 0, 1, '0', '0', 0, '', '', NULL),
(15, '1', 10, '', '', '123456789', '', '2017-07-07 18:08:00', 189.5, '189.50', 189.5, NULL, NULL, '2017-07-07 18:08:33', NULL, 0, 10, 'sales', '0000-00-00 00:00:00', 0, 1, '0', '0', 0, '', '', NULL),
(16, '1', 11, '', '', '123456789', '', '2017-07-07 18:09:00', 200, '200', 200, NULL, NULL, '2017-07-07 18:09:30', NULL, 0, 0, 'sales', '0000-00-00 00:00:00', 0, 1, '0', '0', 0, '', '', NULL),
(17, '1', 12, '', '', '123456789', '', '2017-07-10 15:28:00', 350, '-50', -50, NULL, NULL, '2017-07-10 15:28:26', NULL, 0, 0, 'sales', '0000-00-00 00:00:00', 0, 1, '0', '-400', 0, '', '', NULL),
(18, '1', 13, 'Sooryadas', '9633919919', '', '', '2017-07-11 14:31:00', 750, '755', 755, NULL, NULL, '2017-07-11 14:37:30', NULL, 0, 0, 'sales', '0000-00-00 00:00:00', 0, 0, '5', '0', 0, '', '', 'KL'),
(19, '1', 14, 'sooryadas', '7896541230', '', '', '2017-07-11 14:38:00', 199.45, '203.92999999999998', 203.93, NULL, NULL, '2017-07-11 14:39:04', NULL, 0, 0.52, 'sales', '0000-00-00 00:00:00', 0, 0, '5', '0', 0, '', '', ''),
(20, '1', 15, 'sooryadas', '1234567890', '', '', '2017-07-11 14:42:00', 199.45, '208.92999999999998', 208.93, NULL, NULL, '2017-07-11 14:42:42', NULL, 0, 0.52, 'sales', '0000-00-00 00:00:00', 0, 0, '10', '0', 0, '', '', ''),
(21, '1', 16, 'sooryadas', '9633919919', '', '', '2017-07-11 14:53:00', 199.45, '209.45', 209.45, NULL, NULL, '2017-07-11 14:54:14', NULL, 0, 0.52, 'sales', '0000-00-00 00:00:00', 0, 0, '10', '0', 0, '', '', ''),
(22, '1', 17, 'sooryadas', '1234567890', '', '', '2017-07-11 15:30:00', 10526, '10526', 10526, NULL, NULL, '2017-07-11 15:34:18', NULL, 0, 0, 'sales', '0000-00-00 00:00:00', 0, 0, '0', '0', 0, '', '', ''),
(23, '1', 18, '', '', '123456789', '', '2017-07-13 17:34:00', 200, '200', 200, NULL, NULL, '2017-07-13 17:34:59', NULL, 0, 0, 'sales', '0000-00-00 00:00:00', 0, 1, '0', '0', 0, '', '', 'KL'),
(24, '1', 19, '', '', '123456789', '', '2017-07-13 17:35:00', 400, '400', 400, NULL, NULL, '2017-07-13 17:35:53', NULL, 0, 0, 'sales', '0000-00-00 00:00:00', 0, 1, '0', '0', 0, '', '', 'KL'),
(25, '1', 20, '', '', '123456789', '', '2017-07-13 19:41:00', 1350, '1350', 1350, NULL, NULL, '2017-07-13 19:42:02', NULL, 0, 0, 'sales', '0000-00-00 00:00:00', 0, 1, '0', '0', 0, '', '', 'KL'),
(26, '1', 21, '', '', '', '', '2017-07-15 11:26:00', 1800.04, '1800', 1800, NULL, NULL, '2017-07-15 11:27:31', NULL, 0, 190.48, 'sales', '0000-00-00 00:00:00', 0, 0, '0', '0', 0, '', '', ''),
(27, '1', 22, 'SOORYADAS', '9633919919', '', '', '2017-07-15 15:25:00', 1980.04, '1967', 1967, NULL, NULL, '2017-07-15 15:26:38', NULL, 0, 32.38, 'sales', '0000-00-00 00:00:00', 0, 0, '0', '0', 0, '', '', ''),
(28, '1', 23, 'DAS', '9633919919', '', '', '2017-07-15 15:29:00', 19000.4, '19000', 19000, NULL, NULL, '2017-07-15 15:30:10', NULL, 0, 952.4, 'sales', '0000-00-00 00:00:00', 0, 0, '0', '0', 0, '', '', 'KA'),
(29, '1', 24, '', '', '', '', '2017-07-17 18:22:00', 200, '200', 200, NULL, NULL, '2017-07-17 18:23:04', NULL, 0, 0, 'sales', '0000-00-00 00:00:00', 0, 0, '0', '0', 0, '', '', 'GA'),
(30, '1', 25, '', '', '123456789', '', '2017-07-18 16:18:00', 200, '200', 200, NULL, NULL, '2017-07-18 16:19:06', NULL, 0, 10, 'sales', '0000-00-00 00:00:00', 0, 1, '0', '0', 0, '', '', 'KL'),
(31, '1', 26, '', '', '', '', '2017-07-24 08:51:00', 368, '368', 368, NULL, NULL, '2017-07-24 08:52:07', NULL, 0, 0, 'sales', '0000-00-00 00:00:00', 0, 0, '0', '0', 0, '', '', ''),
(32, '1', 27, '', '', '', '', '2017-07-24 08:53:00', 368, '268', 268, NULL, NULL, '2017-07-24 08:53:31', NULL, 0, 0, 'sales', '0000-00-00 00:00:00', 0, 10, '0', '-100', 0, '', '', 'KL'),
(33, '1', 28, '', '', '', '', '2017-07-24 08:54:00', 315, '315', 315, NULL, NULL, '2017-07-24 08:58:37', NULL, 0, 0, 'sales', '0000-00-00 00:00:00', 0, 10, '0', '0', 0, '', '', 'KL'),
(34, '1', 29, '', '', '123456789', '', '2017-07-24 09:27:00', 336, '336', 336, NULL, NULL, '2017-07-24 09:27:54', NULL, 0, 0, 'sales', '0000-00-00 00:00:00', 0, 1, '0', '0', 0, '', '', 'KL'),
(35, '1', 30, '', '', '', '', '2017-07-24 09:28:00', 410, '410', 410, NULL, NULL, '2017-07-24 09:28:47', NULL, 0, 0, 'sales', '0000-00-00 00:00:00', 0, 10, '0', '0', 0, '', '', 'KL');

-- --------------------------------------------------------

--
-- Table structure for table `vm_billitems`
--

CREATE TABLE `vm_billitems` (
  `bi_billitemid` int(11) NOT NULL,
  `user_id` varchar(100) NOT NULL,
  `bi_billid` int(11) DEFAULT NULL,
  `bi_productid` int(11) DEFAULT NULL,
  `bi_price` float DEFAULT NULL,
  `bi_quantity` float DEFAULT NULL,
  `bi_taxamount` varchar(10) NOT NULL,
  `bi_discount` varchar(10) NOT NULL,
  `bi_total` float DEFAULT NULL,
  `bi_updatedon` datetime DEFAULT NULL,
  `bi_isactive` int(11) DEFAULT '0',
  `bi_vatamount` float DEFAULT NULL,
  `bi_vatper` float DEFAULT NULL,
  `be_coolie` varchar(100) NOT NULL,
  `bi_sgst` float DEFAULT NULL,
  `bi_sgst_amt` float DEFAULT NULL,
  `bi_cgst` float DEFAULT NULL,
  `bi_cgst_amt` float DEFAULT NULL,
  `bi_igst` float DEFAULT NULL,
  `bi_igst_amt` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vm_billitems`
--

INSERT INTO `vm_billitems` (`bi_billitemid`, `user_id`, `bi_billid`, `bi_productid`, `bi_price`, `bi_quantity`, `bi_taxamount`, `bi_discount`, `bi_total`, `bi_updatedon`, `bi_isactive`, `bi_vatamount`, `bi_vatper`, `be_coolie`, `bi_sgst`, `bi_sgst_amt`, `bi_cgst`, `bi_cgst_amt`, `bi_igst`, `bi_igst_amt`) VALUES
(1, '1', 1, 1, 200, 10, '', '', 2100, '2017-06-30 15:57:11', 0, 100, 5, '', NULL, NULL, NULL, NULL, NULL, NULL),
(2, '1', 2, 1, 500, 1, '', '', 525, '2017-06-30 15:58:52', 0, 25, 5, '', NULL, NULL, NULL, NULL, NULL, NULL),
(3, '1', 3, 1, 1000, 1, '', '', 1050, '2017-06-30 16:03:03', 0, 50, 5, '', NULL, NULL, NULL, NULL, NULL, NULL),
(4, '1', 22, 1, 1000, 1, '', '', 1050, '2017-06-30 16:05:15', 0, 50, 5, '', NULL, NULL, NULL, NULL, NULL, NULL),
(5, '1', 22, 1, 190.48, 1, '', '', 200, '2017-07-05 03:30:30', 0, NULL, NULL, '', 2.5, 4.76, 2.5, 4.76, 0, 0),
(6, '1', 22, 1, 190.48, 1, '', '', 200, '2017-07-05 03:31:14', 0, NULL, NULL, '', 0, 0, 0, 0, 5, 9.52),
(7, '1', 22, 1, 190.48, 1, '', '10', 189.5, '2017-07-05 03:37:58', 0, NULL, NULL, '', 2.5, 4.51, 2.5, 4.51, 0, 0),
(8, '1', 22, 3, 333.33, 1, '', '5', 344.75, '2017-07-05 03:37:58', 0, NULL, NULL, '', 2.5, 8.21, 2.5, 8.21, 0, 0),
(9, '1', 9, 1, 190.48, 1, '', '', 200, '2017-07-05 05:23:12', 0, NULL, NULL, '', 2.5, 4.76, 2.5, 4.76, 0, 0),
(10, '1', 10, 1, 190.48, 1, '', '', 200, '2017-07-05 05:26:17', 0, NULL, NULL, '', 2.5, 4.76, 2.5, 4.76, 0, 0),
(11, '1', 11, 1, 190.48, 1, '', '', 200, '2017-07-06 10:00:05', 0, NULL, NULL, '', 0, 0, 0, 0, 5, 9.52),
(12, '1', 12, 1, 190.48, 1, '', '', 200, '2017-07-07 14:38:49', 0, NULL, NULL, '', 2.5, 4.76, 2.5, 4.76, 0, 0),
(13, '1', 13, 1, 190.48, 1, '', '', 200, '2017-07-07 17:49:24', 0, NULL, NULL, '', 2.5, 4.76, 2.5, 4.76, 0, 0),
(14, '1', 14, 1, 190.48, 1, '', '', 200, '2017-07-07 17:49:39', 0, NULL, NULL, '', 0, 0, 0, 0, 5, 9.52),
(15, '1', 15, 1, 190.48, 1, '', '', 189.5, '2017-07-07 18:08:33', 0, NULL, NULL, '', 2.5, 4.51, 2.5, 4.51, 0, 0),
(16, '1', 16, 1, 190.48, 1, '', '', 200, '2017-07-07 18:09:30', 0, NULL, NULL, '', 0, 0, 0, 0, 5, 9.52),
(17, '1', 17, 3, 333.33, 1, '', '', 350, '2017-07-10 15:28:26', 0, NULL, NULL, '', 2.5, 8.335, 2.5, 8.335, 0, 0),
(18, '1', 18, 1, 190.48, 1, '', '', 200, '2017-07-11 14:37:30', 0, NULL, NULL, '', 2.5, 4.76, 2.5, 4.76, 0, 0),
(19, '1', 19, 1, 190.48, 1, '', '', 199.45, '2017-07-11 14:39:04', 0, NULL, NULL, '', 2.5, 4.75, 2.5, 4.75, 0, 0),
(20, '1', 20, 1, 190.48, 1, '', '1', 199.45, '2017-07-11 14:42:42', 0, NULL, NULL, '', 2.5, 4.75, 2.5, 4.75, 0, 0),
(21, '1', 21, 1, 190.48, 1, '', '1', 199.45, '2017-07-11 14:54:14', 0, NULL, NULL, '', 2.5, 4.75, 2.5, 4.75, 0, 0),
(22, '1', 22, 1, 190.48, 1, '', '', 200, '2017-07-11 15:34:18', 0, NULL, NULL, '', 2.5, 4.76, 2.5, 4.76, 0, 0),
(23, '1', 22, 3, 333.33, 1, '', '', 350, '2017-07-11 15:34:18', 0, NULL, NULL, '', 2.5, 8.335, 2.5, 8.335, 0, 0),
(24, '1', 22, 2, 380.95, 1, '', '', 400, '2017-07-11 15:34:18', 0, NULL, NULL, '', 2.5, 9.525, 2.5, 9.525, 0, 0),
(25, '1', 22, 4, 380.95, 1, '', '', 400, '2017-07-11 15:34:18', 0, NULL, NULL, '', 2.5, 9.525, 2.5, 9.525, 0, 0),
(26, '1', 22, 6, 142.86, 1, '', '', 150, '2017-07-11 15:34:18', 0, NULL, NULL, '', 2.5, 3.57, 2.5, 3.57, 0, 0),
(27, '1', 22, 5, 952.38, 1, '', '', 1000, '2017-07-11 15:34:18', 0, NULL, NULL, '', 2.5, 23.81, 2.5, 23.81, 0, 0),
(28, '1', 22, 7, 143.81, 1, '', '', 151, '2017-07-11 15:34:18', 0, NULL, NULL, '', 2.5, 3.595, 2.5, 3.595, 0, 0),
(29, '1', 22, 8, 144.76, 1, '', '', 152, '2017-07-11 15:34:18', 0, NULL, NULL, '', 2.5, 3.62, 2.5, 3.62, 0, 0),
(30, '1', 22, 9, 143.81, 1, '', '', 151, '2017-07-11 15:34:18', 0, NULL, NULL, '', 2.5, 3.595, 2.5, 3.595, 0, 0),
(31, '1', 22, 10, 144.76, 1, '', '', 152, '2017-07-11 15:34:18', 0, NULL, NULL, '', 2.5, 3.62, 2.5, 3.62, 0, 0),
(32, '1', 22, 11, 145.71, 1, '', '', 153, '2017-07-11 15:34:18', 0, NULL, NULL, '', 2.5, 3.645, 2.5, 3.645, 0, 0),
(33, '1', 22, 12, 146.67, 1, '', '', 154, '2017-07-11 15:34:18', 0, NULL, NULL, '', 2.5, 3.665, 2.5, 3.665, 0, 0),
(34, '1', 22, 14, 380.95, 1, '', '', 400, '2017-07-11 15:34:18', 0, NULL, NULL, '', 2.5, 9.525, 2.5, 9.525, 0, 0),
(35, '1', 22, 15, 333.33, 1, '', '', 350, '2017-07-11 15:34:18', 0, NULL, NULL, '', 2.5, 8.335, 2.5, 8.335, 0, 0),
(36, '1', 22, 18, 142.86, 1, '', '', 150, '2017-07-11 15:34:18', 0, NULL, NULL, '', 2.5, 3.57, 2.5, 3.57, 0, 0),
(37, '1', 22, 17, 952.38, 1, '', '', 1000, '2017-07-11 15:34:18', 0, NULL, NULL, '', 2.5, 23.81, 2.5, 23.81, 0, 0),
(38, '1', 22, 16, 380.95, 1, '', '', 400, '2017-07-11 15:34:18', 0, NULL, NULL, '', 2.5, 9.525, 2.5, 9.525, 0, 0),
(39, '1', 22, 19, 143.81, 1, '', '', 151, '2017-07-11 15:34:18', 0, NULL, NULL, '', 2.5, 3.595, 2.5, 3.595, 0, 0),
(40, '1', 22, 20, 144.76, 1, '', '', 152, '2017-07-11 15:34:18', 0, NULL, NULL, '', 2.5, 3.62, 2.5, 3.62, 0, 0),
(41, '1', 22, 21, 143.81, 1, '', '', 151, '2017-07-11 15:34:18', 0, NULL, NULL, '', 2.5, 3.595, 2.5, 3.595, 0, 0),
(42, '1', 22, 22, 144.76, 1, '', '', 152, '2017-07-11 15:34:18', 0, NULL, NULL, '', 2.5, 3.62, 2.5, 3.62, 0, 0),
(43, '1', 22, 23, 145.71, 1, '', '', 153, '2017-07-11 15:34:18', 0, NULL, NULL, '', 2.5, 3.645, 2.5, 3.645, 0, 0),
(44, '1', 22, 24, 146.67, 1, '', '', 154, '2017-07-11 15:34:18', 0, NULL, NULL, '', 2.5, 3.665, 2.5, 3.665, 0, 0),
(45, '1', 22, 25, 190.48, 1, '', '', 200, '2017-07-11 15:34:18', 0, NULL, NULL, '', 2.5, 4.76, 2.5, 4.76, 0, 0),
(46, '1', 22, 26, 380.95, 1, '', '', 400, '2017-07-11 15:34:18', 0, NULL, NULL, '', 2.5, 9.525, 2.5, 9.525, 0, 0),
(47, '1', 22, 27, 333.33, 1, '', '', 350, '2017-07-11 15:34:18', 0, NULL, NULL, '', 2.5, 8.335, 2.5, 8.335, 0, 0),
(48, '1', 22, 28, 380.95, 1, '', '', 400, '2017-07-11 15:34:18', 0, NULL, NULL, '', 2.5, 9.525, 2.5, 9.525, 0, 0),
(49, '1', 22, 29, 952.38, 1, '', '', 1000, '2017-07-11 15:34:18', 0, NULL, NULL, '', 2.5, 23.81, 2.5, 23.81, 0, 0),
(50, '1', 22, 30, 142.86, 1, '', '', 150, '2017-07-11 15:34:18', 0, NULL, NULL, '', 2.5, 3.57, 2.5, 3.57, 0, 0),
(51, '1', 23, 1, 190.48, 1, '', '', 200, '2017-07-13 17:34:59', 0, NULL, NULL, '', 2.5, 4.76, 2.5, 4.76, 0, 0),
(52, '1', 24, 1, 190.48, 2, '', '', 400, '2017-07-13 17:35:53', 0, NULL, NULL, '', 2.5, 9.525, 2.5, 9.525, 0, 0),
(53, '1', 25, 1, 190.48, 1, '', '', 200, '2017-07-13 19:42:02', 0, NULL, NULL, '', 2.5, 4.76, 2.5, 4.76, 0, 0),
(54, '1', 25, 3, 333.33, 1, '', '', 350, '2017-07-13 19:42:02', 0, NULL, NULL, '', 2.5, 8.335, 2.5, 8.335, 0, 0),
(55, '1', 25, 2, 380.95, 1, '', '', 400, '2017-07-13 19:42:02', 0, NULL, NULL, '', 2.5, 9.525, 2.5, 9.525, 0, 0),
(56, '1', 25, 4, 380.95, 1, '', '', 400, '2017-07-13 19:42:02', 0, NULL, NULL, '', 2.5, 9.525, 2.5, 9.525, 0, 0),
(57, '1', 26, 1, 190.48, 10, '', '10', 1800.04, '2017-07-15 11:27:31', 0, NULL, NULL, '', 2.5, 42.86, 2.5, 42.86, 0, 0),
(58, '1', 27, 1, 190.48, 10, '', '1', 1980.04, '2017-07-15 15:26:38', 0, NULL, NULL, '', 2.5, 47.145, 2.5, 47.145, 0, 0),
(59, '1', 28, 1, 190.48, 100, '', '5', 19000.4, '2017-07-15 15:30:10', 0, NULL, NULL, '', 0, 0, 0, 0, 5, 904.78),
(60, '1', 29, 1, 190.48, 1, '', '', 200, '2017-07-17 18:23:04', 0, NULL, NULL, '', 0, 0, 0, 0, 5, 9.52),
(61, '1', 31, 3, 350, 1, '350', '', 368, '2017-07-24 08:52:07', 0, NULL, NULL, '', 2.5, 8.75, 2.5, 8.75, 0, 0),
(62, '1', 32, 3, 350, 1, '350', '', 368, '2017-07-24 08:53:31', 0, NULL, NULL, '', 2.5, 8.75, 2.5, 8.75, 0, 0),
(63, '1', 33, 3, 300, 1, '300', '', 315, '2017-07-24 08:58:37', 0, NULL, NULL, '', 2.5, 7.5, 2.5, 7.5, 0, 0),
(64, '1', 34, 3, 320, 1, '320', '', 336, '2017-07-24 09:27:54', 0, NULL, NULL, '', 2.5, 8, 2.5, 8, 0, 0),
(65, '1', 35, 2, 390, 1, '390', '', 410, '2017-07-24 09:28:47', 0, NULL, NULL, '', 2.5, 9.75, 2.5, 9.75, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `vm_catogory`
--

CREATE TABLE `vm_catogory` (
  `ca_categoryid` int(11) NOT NULL,
  `user_id` varchar(100) NOT NULL,
  `ca_categoryname` varchar(200) DEFAULT NULL,
  `ca_vat` varchar(200) DEFAULT NULL,
  `ca_isactive` int(11) DEFAULT '0',
  `ca_updatedtime` datetime DEFAULT NULL,
  `ca_note` longtext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vm_catogory`
--

INSERT INTO `vm_catogory` (`ca_categoryid`, `user_id`, `ca_categoryname`, `ca_vat`, `ca_isactive`, `ca_updatedtime`, `ca_note`) VALUES
(1, '1', 'category1', '5', 0, '2017-06-20 12:05:35', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `vm_customer`
--

CREATE TABLE `vm_customer` (
  `cs_customerid` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `cs_billnumber` int(11) DEFAULT NULL,
  `cs_customername` varchar(600) DEFAULT NULL,
  `cs_customerphone` varchar(100) DEFAULT NULL,
  `cs_address` varchar(1000) DEFAULT NULL,
  `cs_email` varchar(400) DEFAULT NULL,
  `cs_tin_number` varchar(100) DEFAULT NULL,
  `cs_payment` float DEFAULT NULL,
  `cs_discount` float DEFAULT NULL,
  `cs_paid` float DEFAULT NULL,
  `cs_balance` float DEFAULT NULL,
  `cs_paymethod` varchar(250) DEFAULT NULL,
  `cs_note` longtext,
  `cs_updateddate` datetime DEFAULT NULL,
  `cs_updatedby` varchar(250) DEFAULT NULL,
  `cs_isactive` int(11) DEFAULT '0',
  `cs_acntid` varchar(100) NOT NULL,
  `cs_statecode` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vm_customer`
--

INSERT INTO `vm_customer` (`cs_customerid`, `user_id`, `cs_billnumber`, `cs_customername`, `cs_customerphone`, `cs_address`, `cs_email`, `cs_tin_number`, `cs_payment`, `cs_discount`, `cs_paid`, `cs_balance`, `cs_paymethod`, `cs_note`, `cs_updateddate`, `cs_updatedby`, `cs_isactive`, `cs_acntid`, `cs_statecode`) VALUES
(1, 1, NULL, 'CUSTOMER', '9876543210', 'PALAKKAD', 'customer@gmail.com', '123456789', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, '11', 'KL'),
(5, 1, NULL, 'ajimsha', '9638527410', 'palakkad', '', '', NULL, NULL, NULL, 50, NULL, NULL, NULL, NULL, 0, '3', NULL),
(6, 1, NULL, 'amritha', '98745463210', '', '', '', NULL, NULL, NULL, 50, NULL, NULL, NULL, NULL, 0, '4', NULL),
(7, 1, NULL, 'amritha', '', '', '', '', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, '5', NULL),
(8, 1, NULL, 'das', '', '', '', '', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, '6', NULL),
(9, 1, NULL, 'das123', '', '', '', '', NULL, NULL, NULL, 25, NULL, NULL, NULL, NULL, 0, '7', 'KL'),
(10, 1, NULL, 'sooryadas', '9633919919', 'palakkad', '', '', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, '28', 'KL'),
(11, 1, NULL, 'sdfs', '', '', '', '', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, '', 'KL');

-- --------------------------------------------------------

--
-- Table structure for table `vm_customeritemprice`
--

CREATE TABLE `vm_customeritemprice` (
  `cp_id` int(11) NOT NULL,
  `cp_customer_id` varchar(10) DEFAULT NULL,
  `cp_itemid` varchar(10) DEFAULT NULL,
  `cp_itemprice` varchar(100) DEFAULT NULL,
  `cp_isactive` int(11) DEFAULT '0',
  `cp_updateddate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vm_customeritemprice`
--

INSERT INTO `vm_customeritemprice` (`cp_id`, `cp_customer_id`, `cp_itemid`, `cp_itemprice`, `cp_isactive`, `cp_updateddate`) VALUES
(1, '10', '3', '300', 0, '2017-07-24 08:53:31'),
(2, '1', '3', '320', 0, '2017-07-24 09:27:54'),
(3, '10', '2', '390', 0, '2017-07-24 09:28:47');

-- --------------------------------------------------------

--
-- Table structure for table `vm_estimation`
--

CREATE TABLE `vm_estimation` (
  `be_billid` int(11) NOT NULL,
  `user_id` varchar(100) NOT NULL,
  `be_billnumber` int(11) DEFAULT NULL,
  `be_customername` varchar(600) DEFAULT NULL,
  `be_customermobile` varchar(20) DEFAULT NULL,
  `be_customer_tin_num` varchar(100) NOT NULL,
  `be_vehicle_number` varchar(10) NOT NULL,
  `be_billdate` datetime DEFAULT NULL,
  `be_total` float DEFAULT NULL,
  `be_gtotal` varchar(100) NOT NULL,
  `be_paidamount` float DEFAULT NULL,
  `be_paymethod` varchar(250) DEFAULT NULL,
  `be_note` longtext,
  `be_updateddate` datetime DEFAULT NULL,
  `be_updatedby` varchar(250) DEFAULT NULL,
  `be_isactive` int(11) DEFAULT '0',
  `be_discount` float DEFAULT NULL,
  `be_mode` varchar(100) NOT NULL,
  `be_paydate` datetime NOT NULL,
  `be_balance` int(11) NOT NULL,
  `be_customerid` int(11) NOT NULL,
  `be_coolie` varchar(100) NOT NULL,
  `be_oldbal` varchar(100) NOT NULL,
  `be_totvat` float NOT NULL,
  `be_debitid` varchar(100) NOT NULL COMMENT 'cash sales',
  `be_creditid` varchar(100) NOT NULL COMMENT 'credit sales',
  `be_statecode` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vm_estimation`
--

INSERT INTO `vm_estimation` (`be_billid`, `user_id`, `be_billnumber`, `be_customername`, `be_customermobile`, `be_customer_tin_num`, `be_vehicle_number`, `be_billdate`, `be_total`, `be_gtotal`, `be_paidamount`, `be_paymethod`, `be_note`, `be_updateddate`, `be_updatedby`, `be_isactive`, `be_discount`, `be_mode`, `be_paydate`, `be_balance`, `be_customerid`, `be_coolie`, `be_oldbal`, `be_totvat`, `be_debitid`, `be_creditid`, `be_statecode`) VALUES
(1, '1', 6, '', '', '', '', '2017-07-05 05:53:00', 200, '200', 200, NULL, NULL, '2017-07-05 05:54:10', NULL, 0, 0, 'sales', '0000-00-00 00:00:00', 0, 10, '0', '', 0, '', '', ' '),
(2, '1', 7, '', '', '', '', '2017-07-05 11:50:00', 550, '550', 550, NULL, NULL, '2017-07-05 11:51:12', NULL, 0, 0, 'sales', '0000-00-00 00:00:00', 0, 0, '0', '', 0, '', '', ' '),
(3, '1', 8, '', '', '', '', '2017-07-05 11:53:00', 2400, '2400', 2400, NULL, NULL, '2017-07-05 11:54:09', NULL, 0, 0, 'sales', '0000-00-00 00:00:00', 0, 0, '0', '', 0, '', '', ' '),
(4, '1', 9, 'das', '', '', '', '2017-07-10 13:30:00', 350, '350', 350, NULL, NULL, '2017-07-10 13:30:20', NULL, 0, 0, 'sales', '0000-00-00 00:00:00', 0, 0, '0', '', 0, '', '', ' '),
(5, '1', 10, 'das', '', '', '', '2017-07-10 13:32:00', 200, '200', 200, NULL, NULL, '2017-07-10 13:32:40', NULL, 0, 0, 'sales', '0000-00-00 00:00:00', 0, 0, '0', '', 0, '', '', ' ');

-- --------------------------------------------------------

--
-- Table structure for table `vm_estimationitems`
--

CREATE TABLE `vm_estimationitems` (
  `bi_billitemid` int(11) NOT NULL,
  `user_id` varchar(100) NOT NULL,
  `bi_billid` int(11) DEFAULT NULL,
  `bi_productid` int(11) DEFAULT NULL,
  `bi_price` float DEFAULT NULL,
  `bi_quantity` float DEFAULT NULL,
  `bi_discount` varchar(10) NOT NULL,
  `bi_total` float DEFAULT NULL,
  `bi_updatedon` datetime DEFAULT NULL,
  `bi_isactive` int(11) DEFAULT '0',
  `bi_vatamount` float DEFAULT NULL,
  `bi_vatper` float DEFAULT NULL,
  `be_coolie` varchar(100) NOT NULL,
  `bi_sgst` float DEFAULT NULL,
  `bi_sgst_amt` float DEFAULT NULL,
  `bi_cgst` float DEFAULT NULL,
  `bi_cgst_amt` float DEFAULT NULL,
  `bi_igst` float DEFAULT NULL,
  `bi_igst_amt` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vm_estimationitems`
--

INSERT INTO `vm_estimationitems` (`bi_billitemid`, `user_id`, `bi_billid`, `bi_productid`, `bi_price`, `bi_quantity`, `bi_discount`, `bi_total`, `bi_updatedon`, `bi_isactive`, `bi_vatamount`, `bi_vatper`, `be_coolie`, `bi_sgst`, `bi_sgst_amt`, `bi_cgst`, `bi_cgst_amt`, `bi_igst`, `bi_igst_amt`) VALUES
(1, '1', 1, 1, 190.48, 1, '', 200, '2017-07-05 05:54:10', 0, NULL, NULL, '', 0, 0, 0, 0, 0, 0),
(2, '1', 2, 1, 190.48, 1, '', 200, '2017-07-05 11:51:12', 0, NULL, NULL, '', 0, 0, 0, 0, 0, 0),
(3, '1', 2, 3, 333.33, 1, '', 350, '2017-07-05 11:51:12', 0, NULL, NULL, '', 0, 0, 0, 0, 0, 0),
(4, '1', 3, 1, 190.48, 12, '0', 2400, '2017-07-05 11:54:09', 0, NULL, NULL, '', 0, 0, 0, 0, 0, 0),
(5, '1', 4, 3, 333.33, 1, '', 350, '2017-07-10 13:30:20', 0, NULL, NULL, '', 0, 0, 0, 0, 0, 0),
(6, '1', 5, 1, 190.48, 1, '', 200, '2017-07-10 13:32:40', 0, NULL, NULL, '', 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `vm_payment`
--

CREATE TABLE `vm_payment` (
  `pa_paymentid` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `pa_customerid` int(11) DEFAULT NULL,
  `pa_billid` int(11) DEFAULT NULL,
  `pa_balance` float DEFAULT NULL,
  `pa_newpayment` float DEFAULT NULL,
  `pa_newbalance` float DEFAULT NULL,
  `pa_isactive` int(11) DEFAULT '0',
  `pa_updatedon` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vm_payment`
--

INSERT INTO `vm_payment` (`pa_paymentid`, `user_id`, `pa_customerid`, `pa_billid`, `pa_balance`, `pa_newpayment`, `pa_newbalance`, `pa_isactive`, `pa_updatedon`) VALUES
(1, 1, 9, 0, 50, 10, 40, 0, '2017-07-10 15:55:55'),
(2, 1, 9, 0, 40, 10, 30, 0, '2017-07-10 16:29:29'),
(3, 1, 0, 0, 30, 5, 25, 0, '2017-07-10 16:33:16'),
(4, 1, 0, 0, 30, 5, 25, 0, '2017-07-10 16:49:31'),
(5, 1, 9, 0, 30, 5, 25, 0, '2017-07-10 16:51:20');

-- --------------------------------------------------------

--
-- Table structure for table `vm_products`
--

CREATE TABLE `vm_products` (
  `pr_productid` int(11) NOT NULL,
  `user_id` varchar(100) NOT NULL,
  `pr_productcode` varchar(100) DEFAULT NULL,
  `pr_productname` varchar(1000) DEFAULT NULL,
  `pr_hsn` varchar(1000) DEFAULT NULL,
  `pr_purchaseprice` float DEFAULT NULL,
  `pr_saleprice` float DEFAULT NULL,
  `pr_description` longtext,
  `pr_stock` float DEFAULT NULL,
  `pr_isactive` int(11) DEFAULT '0',
  `pr_updateddate` datetime DEFAULT NULL,
  `pr_type` int(11) DEFAULT NULL,
  `pr_unit` varchar(700) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vm_products`
--

INSERT INTO `vm_products` (`pr_productid`, `user_id`, `pr_productcode`, `pr_productname`, `pr_hsn`, `pr_purchaseprice`, `pr_saleprice`, `pr_description`, `pr_stock`, `pr_isactive`, `pr_updateddate`, `pr_type`, `pr_unit`) VALUES
(2, '1', '2000', 'item2', '48848489', 200, 400, NULL, 7, 0, '2017-06-20 12:06:27', 1, 'Piece'),
(3, '1', '1001', 'item3', '987456321', 300, 350, NULL, 2, 0, '2017-07-05 00:36:06', 1, 'Kg'),
(5, '1', '2004', 'item4', '125', 100, 1000, NULL, 9, 0, '2017-07-05 11:50:11', 1, 'Piece'),
(6, '1', '2005', 'item5', '123456', 100, 150, NULL, 9, 0, '2017-07-06 13:44:06', 1, 'Kg'),
(7, '1', '2006', 'item6', '123457', 101, 151, NULL, 999, 0, '2017-07-06 13:44:06', 1, 'Kg'),
(8, '1', '2007', 'item7', '12348', 102, 152, NULL, 9, 0, '2017-07-06 13:44:06', 1, 'Kg'),
(9, '1', '2008', 'item8', '123410', 100, 151, NULL, 9, 0, '2017-07-06 13:45:18', 1, 'Kg'),
(10, '1', '2009', 'item9', '412575', 101, 152, NULL, 9, 0, '2017-07-06 13:45:18', 1, 'Kg'),
(11, '1', '2010', 'item10', '78632', 102, 153, NULL, 9, 0, '2017-07-06 13:45:18', 1, 'Kg'),
(12, '1', '2011', 'item11', '716373', 103, 154, NULL, 9, 0, '2017-07-06 13:45:18', 1, 'Kg'),
(13, '1', '2011', 'item12', '1234565', 100, 200, NULL, 76, 0, '2020-06-17 12:06:00', 1, 'Kg'),
(14, '1', '2012', 'item13', '48848489', 200, 400, NULL, 9, 0, '2020-06-17 12:06:00', 1, 'Piece'),
(15, '1', '2013', 'item14', '987456321', 300, 350, NULL, 8, 0, '2005-07-17 00:36:00', 1, 'Kg'),
(17, '1', '2015', 'item16', '125', 100, 1000, NULL, 9, 0, '2005-07-17 11:50:00', 1, 'Piece'),
(18, '1', '2016', 'item17', '123456', 100, 150, NULL, 9, 0, '2006-07-17 13:44:00', 1, 'Kg'),
(19, '1', '2017', 'item18', '123457', 101, 151, NULL, 999, 0, '2006-07-17 13:44:00', 1, 'Kg'),
(20, '1', '2018', 'item19', '12348', 102, 152, NULL, 9, 0, '2006-07-17 13:44:00', 1, 'Kg'),
(21, '1', '2019', 'item20', '123410', 100, 151, NULL, 9, 0, '2006-07-17 13:45:00', 1, 'Kg'),
(22, '1', '2020', 'item21', '412575', 101, 152, NULL, 9, 0, '2006-07-17 13:45:00', 1, 'Kg'),
(23, '1', '2021', 'item22', '78632', 102, 153, NULL, 9, 0, '2006-07-17 13:45:00', 1, 'Kg'),
(24, '1', '2022', 'item23', '716373', 103, 154, NULL, 9, 0, '2006-07-17 13:45:00', 1, 'Kg'),
(25, '1', '2023', 'item24', '1234565', 100, 200, NULL, 75, 0, '2020-06-17 12:06:00', 1, 'Kg'),
(26, '1', '2024', 'item25', '48848489', 200, 400, NULL, 9, 0, '2020-06-17 12:06:00', 1, 'Piece'),
(27, '1', '2025', 'item26', '987456321', 300, 350, NULL, 8, 0, '2005-07-17 00:36:00', 1, 'Kg'),
(29, '1', '2027', 'item28', '125', 100, 1000, NULL, 9, 0, '2005-07-17 11:50:00', 1, 'Piece'),
(30, '1', '2028', 'item29', '123456', 100, 150, NULL, 9, 0, '2006-07-17 13:44:00', 1, 'Kg'),
(31, '1', '2029', 'item30', '123', 101, 151, NULL, 1000, 0, '2006-07-17 13:44:00', 1, 'Kg'),
(32, '1', '2030', 'item31', '12348', 102, 152, NULL, 10, 0, '2006-07-17 13:44:00', 1, 'Kg'),
(33, '1', '2031', 'item32', '123410', 100, 151, NULL, 10, 0, '2006-07-17 13:45:00', 1, 'Kg'),
(34, '1', '2032', 'item33', '412575', 101, 152, NULL, 10, 0, '2006-07-17 13:45:00', 1, 'Kg'),
(35, '1', '2033', 'item34', '78632', 102, 153, NULL, 10, 0, '2006-07-17 13:45:00', 1, 'Kg'),
(36, '1', '2034', 'item35', '716373', 103, 154, NULL, 10, 0, '2006-07-17 13:45:00', 1, 'Kg'),
(37, '1', '2035', 'item36', '1234565', 100, 200, NULL, 76, 0, '2020-06-17 12:06:00', 1, 'Kg'),
(38, '1', '2036', 'item37', '48848489', 200, 400, NULL, 10, 0, '2020-06-17 12:06:00', 1, 'Piece'),
(39, '1', '2037', 'item38', '987456321', 300, 350, NULL, 9, 0, '2005-07-17 00:36:00', 1, 'Kg'),
(40, '1', '2038', 'item39', '789654123', 350, 400, NULL, 5, 0, '2005-07-17 01:44:00', 1, 'Kg'),
(41, '1', '2039', 'item40', '1251254', 100, 1000, NULL, 10, 0, '2005-07-17 11:50:00', 1, 'Piece'),
(42, '1', '2040', 'item41', '123456', 100, 150, NULL, 10, 0, '2006-07-17 13:44:00', 1, 'Kg'),
(43, '1', '2041', 'item42', '123457', 101, 151, NULL, 1000, 0, '2006-07-17 13:44:00', 1, 'Kg'),
(44, '1', '2042', 'item43', '12348', 102, 152, NULL, 10, 0, '2006-07-17 13:44:00', 1, 'Kg'),
(45, '1', '2043', 'item44', '123410', 100, 151, NULL, 10, 0, '2006-07-17 13:45:00', 1, 'Kg'),
(46, '1', '2044', 'item45', '412575', 101, 152, NULL, 10, 0, '2006-07-17 13:45:00', 1, 'Kg'),
(47, '1', '2045', 'item46', '78632', 102, 153, NULL, 10, 0, '2006-07-17 13:45:00', 1, 'Kg'),
(48, '1', '2046', 'item47', '716373', 103, 154, NULL, 10, 0, '2006-07-17 13:45:00', 1, 'Kg'),
(49, '1', '2047', 'xfcgvhb''cb', '', 45, 50, NULL, 1, 0, '2017-07-24 18:50:05', 1, 'Piece');

-- --------------------------------------------------------

--
-- Table structure for table `vm_purentry`
--

CREATE TABLE `vm_purentry` (
  `pe_billid` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `pe_billnumber` int(11) DEFAULT NULL,
  `pe_customername` varchar(100) DEFAULT NULL,
  `pe_customermobile` int(11) DEFAULT NULL,
  `pe_billdate` datetime DEFAULT NULL,
  `pe_total` float DEFAULT NULL,
  `pe_gtotal` varchar(100) NOT NULL,
  `pe_oldbal` varchar(100) NOT NULL,
  `pe_paidamount` float DEFAULT NULL,
  `pe_paymethod` varchar(200) DEFAULT NULL,
  `pe_note` longtext,
  `pe_updateddate` datetime DEFAULT NULL,
  `pe_updatedby` varchar(250) DEFAULT NULL,
  `pe_isactive` int(11) DEFAULT '0',
  `pe_discount` float DEFAULT NULL,
  `pe_mode` varchar(100) DEFAULT NULL,
  `pe_paydate` datetime DEFAULT NULL,
  `pe_unitprice` int(100) NOT NULL,
  `pe_balance` float NOT NULL,
  `pe_supplierid` varchar(10) NOT NULL,
  `pe_vehicle_number` varchar(100) NOT NULL,
  `pe_invoice_number` varchar(100) NOT NULL,
  `pe_invoice_date` date NOT NULL,
  `pe_statecode` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vm_purentry`
--

INSERT INTO `vm_purentry` (`pe_billid`, `user_id`, `pe_billnumber`, `pe_customername`, `pe_customermobile`, `pe_billdate`, `pe_total`, `pe_gtotal`, `pe_oldbal`, `pe_paidamount`, `pe_paymethod`, `pe_note`, `pe_updateddate`, `pe_updatedby`, `pe_isactive`, `pe_discount`, `pe_mode`, `pe_paydate`, `pe_unitprice`, `pe_balance`, `pe_supplierid`, `pe_vehicle_number`, `pe_invoice_number`, `pe_invoice_date`, `pe_statecode`) VALUES
(1, 1, 1, '', 0, '2017-06-30 16:08:00', 9000, '9000', '', 5000, NULL, NULL, '2017-06-30 16:09:24', NULL, 0, 0, 'purchase', '0000-00-00 00:00:00', 0, 4000, '3', '', '1444', '2017-06-30', NULL),
(2, 1, 2, '', 0, '2017-07-07 14:27:00', 100, '100', '', 100, NULL, NULL, '2017-07-07 14:27:53', NULL, 0, 0, 'purchase', '0000-00-00 00:00:00', 0, 0, '1', '', '', '0000-00-00', ''),
(3, 1, 3, '', 0, '2017-07-07 16:54:00', 100, '100', '', 100, NULL, NULL, '2017-07-07 16:54:40', NULL, 0, 0, 'purchase', '0000-00-00 00:00:00', 0, 0, '1', '', '', '0000-00-00', ''),
(4, 1, 4, '', 0, '2017-07-07 16:54:00', 100, '100', '', 100, NULL, NULL, '2017-07-07 16:54:58', NULL, 0, 0, 'purchase', '0000-00-00 00:00:00', 0, 0, '1', '', '', '0000-00-00', ''),
(5, 1, 5, '', 0, '2017-07-07 16:55:00', 100, '100', '', 100, NULL, NULL, '2017-07-07 16:55:52', NULL, 0, 0, 'purchase', '0000-00-00 00:00:00', 0, 0, '3', '', '', '0000-00-00', ''),
(6, 1, 6, '', 0, '2017-07-07 16:58:00', 100, '100', '', 100, NULL, NULL, '2017-07-07 16:58:27', NULL, 0, 0, 'purchase', '0000-00-00 00:00:00', 0, 0, '1', '', '', '0000-00-00', 'KA'),
(7, 1, 7, '', 0, '2017-07-07 16:58:00', 100, '100', '', 100, NULL, NULL, '2017-07-07 16:58:38', NULL, 0, 0, 'purchase', '0000-00-00 00:00:00', 0, 0, '2', '', '', '0000-00-00', 'KL'),
(8, 1, 8, '', 0, '2017-07-07 16:58:00', 100, '100', '', 100, NULL, NULL, '2017-07-07 16:58:53', NULL, 0, 0, 'purchase', '0000-00-00 00:00:00', 0, 0, '1', '', '', '0000-00-00', 'KL'),
(9, 1, 9, '', 0, '2017-07-07 17:00:00', 100, '100', '', 100, NULL, NULL, '2017-07-07 17:00:57', NULL, 0, 0, 'purchase', '0000-00-00 00:00:00', 0, 0, '3', '', '', '0000-00-00', 'KL'),
(10, 1, 10, '', 0, '2017-07-07 17:02:00', 100, '100', '', 100, NULL, NULL, '2017-07-07 17:02:16', NULL, 0, 0, 'purchase', '0000-00-00 00:00:00', 0, 0, '2', '', '', '0000-00-00', 'KL'),
(11, 1, 11, '', 0, '2017-07-07 17:02:00', 100, '100', '', 100, NULL, NULL, '2017-07-07 17:02:27', NULL, 0, 0, 'purchase', '0000-00-00 00:00:00', 0, 0, '3', '', '', '0000-00-00', 'GA'),
(12, 1, 12, '', 0, '2017-07-07 17:03:00', 300, '300', '', 300, NULL, NULL, '2017-07-07 17:03:31', NULL, 0, 0, 'purchase', '0000-00-00 00:00:00', 0, 0, '2', '', '', '0000-00-00', 'GJ'),
(13, 1, 13, '', 0, '2017-07-07 17:05:00', 100, '100', '', 100, NULL, NULL, '2017-07-07 17:05:42', NULL, 0, 0, 'purchase', '0000-00-00 00:00:00', 0, 0, '2', '', '', '0000-00-00', 'GA'),
(14, 1, 14, '', 0, '2017-07-08 12:27:00', 100, '100', '', 100, NULL, NULL, '2017-07-08 12:27:16', NULL, 0, 0, 'purchase', '0000-00-00 00:00:00', 0, 0, '1', '', '', '0000-00-00', 'GJ'),
(15, 1, 15, '', 0, '2017-07-08 12:34:00', 100, '400', '400', 100, NULL, NULL, '2017-07-08 12:35:19', NULL, 0, 0, 'purchase', '0000-00-00 00:00:00', 0, 0, '1', '', '', '0000-00-00', ''),
(16, 1, 16, '', 0, '2017-07-08 12:37:00', 100, '0', '0', 100, NULL, NULL, '2017-07-08 12:38:39', NULL, 0, 0, 'purchase', '0000-00-00 00:00:00', 0, 0, '1', '', '', '0000-00-00', ''),
(17, 1, 17, '', 0, '2017-07-18 17:42:00', 100, '4000', '3900', 3000, NULL, NULL, '2017-07-18 17:42:41', NULL, 0, 0, 'purchase', '0000-00-00 00:00:00', 0, 1000, '3', '', '', '0000-00-00', 'KL');

-- --------------------------------------------------------

--
-- Table structure for table `vm_puritems`
--

CREATE TABLE `vm_puritems` (
  `pi_billitemid` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `pi_billid` int(11) DEFAULT NULL,
  `pi_productid` int(11) DEFAULT NULL,
  `pi_price` float DEFAULT NULL,
  `pi_quantity` float DEFAULT NULL,
  `pi_total` float DEFAULT NULL,
  `pi_updatedon` datetime DEFAULT NULL,
  `pi_isactive` int(11) DEFAULT '0',
  `pi_vatamount` float DEFAULT NULL,
  `pi_vatper` float DEFAULT NULL,
  `pi_unitprice` int(100) NOT NULL,
  `pi_sgst` float DEFAULT NULL,
  `pi_sgstamt` float DEFAULT NULL,
  `pi_cgst` float DEFAULT NULL,
  `pi_cgstamt` float DEFAULT NULL,
  `pi_igst` float DEFAULT NULL,
  `pi_igstamt` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vm_puritems`
--

INSERT INTO `vm_puritems` (`pi_billitemid`, `user_id`, `pi_billid`, `pi_productid`, `pi_price`, `pi_quantity`, `pi_total`, `pi_updatedon`, `pi_isactive`, `pi_vatamount`, `pi_vatper`, `pi_unitprice`, `pi_sgst`, `pi_sgstamt`, `pi_cgst`, `pi_cgstamt`, `pi_igst`, `pi_igstamt`) VALUES
(1, 1, 1, 1, 95, 90, 9000, '2017-06-30 16:09:24', 0, 450, 5, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 1, 2, 1, 190.48, 1, 100, '2017-07-07 14:27:53', 0, 9.52, 5, 0, 2.5, 4.76, 2.5, 4.76, 0, 0),
(3, 1, 3, 1, 190.48, 1, 100, '2017-07-07 16:54:40', 0, 9.52, 5, 0, 2.5, 4.76, 2.5, 4.76, 0, 0),
(4, 1, 4, 1, 190.48, 1, 100, '2017-07-07 16:54:58', 0, 9.52, 5, 0, 2.5, 4.76, 2.5, 4.76, 0, 0),
(5, 1, 5, 1, 190.48, 1, 100, '2017-07-07 16:55:52', 0, 9.52, 5, 0, 2.5, 4.76, 2.5, 4.76, 0, 0),
(6, 1, 6, 1, 190.48, 1, 100, '2017-07-07 16:58:27', 0, 9.52, 5, 0, 0, 0, 0, 0, 5, 9.52),
(7, 1, 7, 1, 190.48, 1, 100, '2017-07-07 16:58:38', 0, 9.52, 5, 0, 0, 0, 0, 0, 5, 9.52),
(8, 1, 8, 1, 190.48, 1, 100, '2017-07-07 16:58:53', 0, 9.52, 5, 0, 0, 0, 0, 0, 5, 9.52),
(9, 1, 9, 1, 190.48, 1, 100, '2017-07-07 17:00:57', 0, 9.52, 5, 0, 0, 0, 0, 0, 5, 9.52),
(10, 1, 10, 1, 190.48, 1, 100, '2017-07-07 17:02:16', 0, 9.52, 5, 0, 2.5, 4.76, 2.5, 4.76, 0, 0),
(11, 1, 11, 1, 190.48, 1, 100, '2017-07-07 17:02:27', 0, 9.52, 5, 0, 0, 0, 0, 0, 5, 9.52),
(12, 1, 12, 3, 333.33, 1, 300, '2017-07-07 17:03:31', 0, 16.67, 5, 0, 0, 0, 0, 0, 5, 16.67),
(13, 1, 13, 1, 95.24, 1, 100, '2017-07-07 17:05:42', 0, 4.76, 5, 0, 0, 0, 0, 0, 5, 4.76),
(14, 1, 14, 1, 95.24, 1, 100, '2017-07-08 12:27:16', 0, 4.76, 5, 0, 0, 0, 0, 0, 5, 4.76),
(15, 1, 15, 1, 95.24, 1, 100, '2017-07-08 12:35:19', 0, 4.76, 5, 0, 2.5, 2.38, 2.5, 2.38, 0, 0),
(16, 1, 16, 1, 95.24, 1, 100, '2017-07-08 12:38:39', 0, 4.76, 5, 0, 2.5, 2.38, 2.5, 2.38, 0, 0),
(17, 1, 17, 1, 95.24, 1, 100, '2017-07-18 17:42:41', 0, 4.76, 5, 0, 2.5, 2.38, 2.5, 2.38, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `vm_purorder`
--

CREATE TABLE `vm_purorder` (
  `pe_billid` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `pe_billnumber` int(11) DEFAULT NULL,
  `pe_customername` varchar(100) DEFAULT NULL,
  `pe_customermobile` int(11) DEFAULT NULL,
  `pe_billdate` datetime DEFAULT NULL,
  `pe_total` float DEFAULT NULL,
  `pe_gtotal` varchar(100) NOT NULL,
  `pe_oldbal` varchar(100) NOT NULL,
  `pe_paidamount` float DEFAULT NULL,
  `pe_paymethod` varchar(200) DEFAULT NULL,
  `pe_note` longtext,
  `pe_updateddate` datetime DEFAULT NULL,
  `pe_updatedby` varchar(250) DEFAULT NULL,
  `pe_isactive` int(11) DEFAULT '0',
  `pe_discount` float DEFAULT NULL,
  `pe_mode` varchar(100) DEFAULT NULL,
  `pe_paydate` datetime DEFAULT NULL,
  `pe_unitprice` int(100) NOT NULL,
  `pe_balance` float NOT NULL,
  `pe_supplierid` varchar(10) NOT NULL,
  `pe_vehicle_number` varchar(100) NOT NULL,
  `pe_invoice_number` varchar(100) NOT NULL,
  `pe_invoice_date` date NOT NULL,
  `pe_statecode` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vm_purorder`
--

INSERT INTO `vm_purorder` (`pe_billid`, `user_id`, `pe_billnumber`, `pe_customername`, `pe_customermobile`, `pe_billdate`, `pe_total`, `pe_gtotal`, `pe_oldbal`, `pe_paidamount`, `pe_paymethod`, `pe_note`, `pe_updateddate`, `pe_updatedby`, `pe_isactive`, `pe_discount`, `pe_mode`, `pe_paydate`, `pe_unitprice`, `pe_balance`, `pe_supplierid`, `pe_vehicle_number`, `pe_invoice_number`, `pe_invoice_date`, `pe_statecode`) VALUES
(1, 1, 1, '', 0, '2017-06-30 16:08:00', 9000, '9000', '', 5000, NULL, NULL, '2017-06-30 16:09:24', NULL, 0, 0, 'purchase', '0000-00-00 00:00:00', 0, 4000, '3', '', '1444', '2017-06-30', NULL),
(2, 1, 2, '', 0, '2017-07-05 06:52:00', 100, '100', '', 100, NULL, NULL, '2017-07-05 06:52:39', NULL, 0, 0, 'purchase', '0000-00-00 00:00:00', 0, 0, '1', '', '', '0000-00-00', '');

-- --------------------------------------------------------

--
-- Table structure for table `vm_purorderitems`
--

CREATE TABLE `vm_purorderitems` (
  `pi_billitemid` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `pi_billid` int(11) DEFAULT NULL,
  `pi_productid` int(11) DEFAULT NULL,
  `pi_price` float DEFAULT NULL,
  `pi_quantity` float DEFAULT NULL,
  `pi_total` float DEFAULT NULL,
  `pi_updatedon` datetime DEFAULT NULL,
  `pi_isactive` int(11) DEFAULT '0',
  `pi_vatamount` float DEFAULT NULL,
  `pi_vatper` float DEFAULT NULL,
  `pi_unitprice` int(100) NOT NULL,
  `pi_sgst` float DEFAULT NULL,
  `pi_sgstamt` float DEFAULT NULL,
  `pi_cgst` float DEFAULT NULL,
  `pi_cgstamt` float DEFAULT NULL,
  `pi_igst` float DEFAULT NULL,
  `pi_igstamt` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vm_purorderitems`
--

INSERT INTO `vm_purorderitems` (`pi_billitemid`, `user_id`, `pi_billid`, `pi_productid`, `pi_price`, `pi_quantity`, `pi_total`, `pi_updatedon`, `pi_isactive`, `pi_vatamount`, `pi_vatper`, `pi_unitprice`, `pi_sgst`, `pi_sgstamt`, `pi_cgst`, `pi_cgstamt`, `pi_igst`, `pi_igstamt`) VALUES
(1, 1, 1, 1, 95, 90, 9000, '2017-06-30 16:09:24', 0, 450, 5, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 1, 2, 1, 190.48, 1, 100, '2017-07-05 06:52:39', 0, 9.52, 5, 0, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `vm_purreturnentry`
--

CREATE TABLE `vm_purreturnentry` (
  `pre_billid` int(11) NOT NULL,
  `user_id` varchar(100) NOT NULL,
  `pre_billnumber` int(11) DEFAULT NULL,
  `pre_customername` varchar(600) DEFAULT NULL,
  `pre_customermobile` varchar(20) DEFAULT NULL,
  `pre_customer_tin_num` varchar(100) NOT NULL,
  `pre_vehicle_number` varchar(10) NOT NULL,
  `pre_billdate` datetime DEFAULT NULL,
  `pre_total` float DEFAULT NULL,
  `pre_gtotal` float NOT NULL,
  `pre_paidamount` float DEFAULT NULL,
  `pre_paymethod` varchar(250) DEFAULT NULL,
  `pre_note` longtext,
  `pre_updateddate` datetime DEFAULT NULL,
  `pre_updatedby` varchar(250) DEFAULT NULL,
  `pre_isactive` int(11) DEFAULT '0',
  `pre_discount` float DEFAULT NULL,
  `pre_mode` varchar(100) NOT NULL,
  `pre_paydate` datetime NOT NULL,
  `pre_balance` int(11) NOT NULL,
  `pre_customerid` int(11) NOT NULL,
  `pre_coolie` varchar(100) NOT NULL,
  `pre_oldbal` varchar(100) NOT NULL,
  `pre_totvat` float DEFAULT NULL,
  `pre_invoice_number` varchar(100) NOT NULL,
  `pre_invoice_date` date NOT NULL,
  `pre_rebill` varchar(100) DEFAULT NULL,
  `pre_statecode` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vm_purreturnentry`
--

INSERT INTO `vm_purreturnentry` (`pre_billid`, `user_id`, `pre_billnumber`, `pre_customername`, `pre_customermobile`, `pre_customer_tin_num`, `pre_vehicle_number`, `pre_billdate`, `pre_total`, `pre_gtotal`, `pre_paidamount`, `pre_paymethod`, `pre_note`, `pre_updateddate`, `pre_updatedby`, `pre_isactive`, `pre_discount`, `pre_mode`, `pre_paydate`, `pre_balance`, `pre_customerid`, `pre_coolie`, `pre_oldbal`, `pre_totvat`, `pre_invoice_number`, `pre_invoice_date`, `pre_rebill`, `pre_statecode`) VALUES
(1, '1', 1, 'sachin', '', '', '', '2017-07-08 15:38:00', 100, 100, 100, NULL, NULL, '2017-07-08 15:38:18', NULL, 0, 0, 'purchasereturn', '0000-00-00 00:00:00', 3900, 3, '0', '4000', NULL, '', '0000-00-00', '1', ''),
(2, '1', 2, 'COMPANY', '', '', '', '2017-07-08 15:56:00', 100, 100, 100, NULL, NULL, '2017-07-08 15:57:03', NULL, 0, 0, 'purchasereturn', '0000-00-00 00:00:00', -100, 1, '0', '0', NULL, '', '0000-00-00', '2', ''),
(3, '1', 3, 'COMPANY', '', '', '', '2017-07-08 16:03:00', 100, 100, 100, NULL, NULL, '2017-07-08 16:03:27', NULL, 0, 0, 'purchasereturn', '0000-00-00 00:00:00', -200, 1, '0', '-100', NULL, '', '0000-00-00', '2', 'KA'),
(4, '1', 4, 'COMPANY', '', '', '', '2017-07-08 16:15:00', 100, 100, 100, NULL, NULL, '2017-07-08 16:15:10', NULL, 0, 0, 'purchasereturn', '0000-00-00 00:00:00', -300, 1, '0', '-200', NULL, '', '0000-00-00', '2', 'KA'),
(5, '1', 4, 'COMPANY', '', '', '', '2017-07-08 16:15:00', 100, 100, 100, NULL, NULL, '2017-07-08 16:15:54', NULL, 0, 0, 'purchasereturn', '0000-00-00 00:00:00', -300, 1, '0', '-200', NULL, '', '0000-00-00', '2', 'KA'),
(6, '1', 5, 'COMPANY', '', '', '', '2017-07-08 16:16:00', 100, 100, 100, NULL, NULL, '2017-07-08 16:16:49', NULL, 0, 0, 'purchasereturn', '0000-00-00 00:00:00', -400, 1, '0', '-300', NULL, '', '0000-00-00', '2', 'KA'),
(7, '1', 6, 'COMPANY', '', '', '', '2017-07-08 16:18:00', 300, 300, 300, NULL, NULL, '2017-07-08 16:19:01', NULL, 0, 0, 'purchasereturn', '0000-00-00 00:00:00', -700, 1, '0', '-400', NULL, '', '0000-00-00', '2', 'KA'),
(8, '1', 7, 'COMPANY', '', '', '', '2017-07-08 16:20:00', 100, 100, 100, NULL, NULL, '2017-07-08 16:21:12', NULL, 0, 0, 'purchasereturn', '0000-00-00 00:00:00', -800, 1, '0', '-700', NULL, '', '0000-00-00', '2', 'KA'),
(9, '1', 8, 'COMPANY', '', '', '', '2017-07-08 16:44:00', 100, 100, 100, NULL, NULL, '2017-07-08 16:44:52', NULL, 0, 0, 'purchasereturn', '0000-00-00 00:00:00', -900, 1, '0', '-800', NULL, '', '0000-00-00', '2', 'KA'),
(10, '1', 9, 'COMPANY', '', '', '', '2017-07-18 16:40:00', 100, 100, 100, NULL, NULL, '2017-07-18 16:41:58', NULL, 0, 0, 'purchasereturn', '0000-00-00 00:00:00', -1000, 1, '0', '-900', NULL, '', '0000-00-00', '1', 'KA');

-- --------------------------------------------------------

--
-- Table structure for table `vm_purreturnitem`
--

CREATE TABLE `vm_purreturnitem` (
  `pri_billitemid` int(11) NOT NULL,
  `user_id` varchar(100) NOT NULL,
  `pri_billid` int(11) NOT NULL,
  `pri_returnbillid` int(11) NOT NULL,
  `pri_productid` int(11) NOT NULL,
  `pri_price` varchar(100) NOT NULL,
  `pri_quantity` float NOT NULL,
  `pri_total` float NOT NULL,
  `pri_updatedon` datetime NOT NULL,
  `pri_isactive` int(11) NOT NULL,
  `pri_vatamount` float NOT NULL,
  `pri_vatper` float NOT NULL,
  `pri_coolie` float NOT NULL,
  `pri_sgst` float DEFAULT NULL,
  `pri_sgstamt` float DEFAULT NULL,
  `pri_cgst` float DEFAULT NULL,
  `pr_cgstamt` float DEFAULT NULL,
  `pri_igst` float DEFAULT NULL,
  `pri_igstamt` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vm_purreturnitem`
--

INSERT INTO `vm_purreturnitem` (`pri_billitemid`, `user_id`, `pri_billid`, `pri_returnbillid`, `pri_productid`, `pri_price`, `pri_quantity`, `pri_total`, `pri_updatedon`, `pri_isactive`, `pri_vatamount`, `pri_vatper`, `pri_coolie`, `pri_sgst`, `pri_sgstamt`, `pri_cgst`, `pr_cgstamt`, `pri_igst`, `pri_igstamt`) VALUES
(1, '1', 1, 1, 1, '', 1, 100, '2017-07-08 15:38:18', 0, 0, 0, 0, 2.5, 2.38, 2.5, 2.38, 0, 0),
(2, '1', 2, 2, 1, '', 1, 100, '2017-07-08 15:57:03', 0, 0, 0, 0, 2.5, 2.38, 2.5, 2.38, 0, 0),
(3, '1', 3, 2, 1, '', 1, 100, '2017-07-08 16:03:27', 0, 0, 0, 0, 2.5, 2.38, 2.5, 2.38, 0, 0),
(4, '1', 4, 2, 1, '', 1, 100, '2017-07-08 16:15:10', 0, 0, 0, 0, 2.5, 2.38, 2.5, 2.38, 0, 0),
(5, '1', 5, 2, 1, '', 1, 100, '2017-07-08 16:15:54', 0, 0, 0, 0, 2.5, 2.38, 2.5, 2.38, 0, 0),
(6, '1', 6, 2, 1, '', 1, 100, '2017-07-08 16:16:49', 0, 0, 0, 0, 2.5, 2.38, 2.5, 2.38, 0, 0),
(7, '1', 7, 2, 3, '', 1, 300, '2017-07-08 16:19:01', 0, 0, 0, 0, 2.5, 7.145, 2.5, 7.145, 0, 0),
(8, '1', 8, 2, 1, '', 1, 100, '2017-07-08 16:21:12', 0, 0, 0, 0, 0, 0, 0, 0, 5, 4.76),
(9, '1', 9, 2, 1, '95.24', 1, 100, '2017-07-08 16:44:52', 0, 0, 0, 0, 0, 0, 0, 0, 5, 4.76),
(10, '1', 10, 1, 1, '95.24', 1, 100, '2017-07-18 16:41:58', 0, 0, 0, 0, 0, 0, 0, 0, 5, 4.76);

-- --------------------------------------------------------

--
-- Table structure for table `vm_salreturnentry`
--

CREATE TABLE `vm_salreturnentry` (
  `sre_billid` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `sre_billnumber` int(11) DEFAULT NULL,
  `sre_customername` varchar(100) DEFAULT NULL,
  `sre_customeraddress` varchar(200) DEFAULT NULL,
  `sre_customermobile` varchar(100) DEFAULT NULL,
  `sre_customer_tin_num` varchar(100) NOT NULL,
  `sre_billdate` datetime DEFAULT NULL,
  `sre_total` float DEFAULT NULL,
  `sre_gtotal` varchar(100) NOT NULL,
  `sre_oldbal` varchar(100) NOT NULL,
  `sre_paidamount` float DEFAULT NULL,
  `sre_paymethod` varchar(200) DEFAULT NULL,
  `sre_note` longtext,
  `sre_updateddate` datetime DEFAULT NULL,
  `sre_updatedby` varchar(250) DEFAULT NULL,
  `sre_isactive` int(11) DEFAULT '0',
  `sre_discount` float DEFAULT NULL,
  `sre_mode` varchar(100) DEFAULT NULL,
  `sre_paydate` datetime DEFAULT NULL,
  `sre_unitprice` int(100) NOT NULL,
  `sre_balance` float NOT NULL,
  `sre_supplierid` int(10) DEFAULT NULL,
  `sre_vehicle_number` varchar(100) NOT NULL,
  `sre_rebill` varchar(100) DEFAULT NULL,
  `sre_statecode` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vm_salreturnentry`
--

INSERT INTO `vm_salreturnentry` (`sre_billid`, `user_id`, `sre_billnumber`, `sre_customername`, `sre_customeraddress`, `sre_customermobile`, `sre_customer_tin_num`, `sre_billdate`, `sre_total`, `sre_gtotal`, `sre_oldbal`, `sre_paidamount`, `sre_paymethod`, `sre_note`, `sre_updateddate`, `sre_updatedby`, `sre_isactive`, `sre_discount`, `sre_mode`, `sre_paydate`, `sre_unitprice`, `sre_balance`, `sre_supplierid`, `sre_vehicle_number`, `sre_rebill`, `sre_statecode`) VALUES
(1, 1, 5, '', NULL, '', '', '2017-07-01 17:21:00', 1200, '1200', '', 1200, NULL, NULL, '2017-07-01 17:21:57', NULL, 1, 0, 'salesreturn', '0000-00-00 00:00:00', 0, 0, 0, '', '1', NULL),
(2, 1, 6, '', NULL, '', '', '2017-07-08 13:53:00', 200, '200', '100', 200, NULL, NULL, '2017-07-08 13:54:06', NULL, 0, 0, 'salesreturn', '0000-00-00 00:00:00', 0, -100, 10, '', '1', 'KL'),
(3, 1, 7, '', NULL, '', '', '2017-07-08 14:04:00', 200, '200', '0', 200, NULL, NULL, '2017-07-08 14:05:05', NULL, 0, 0, 'salesreturn', '0000-00-00 00:00:00', 0, -200, 1, '', '2', 'KL'),
(4, 1, 8, '', NULL, '', '', '2017-07-08 14:07:00', 200, '200', '-200', 200, NULL, NULL, '2017-07-08 14:07:49', NULL, 0, 0, 'salesreturn', '0000-00-00 00:00:00', 0, -400, 1, '', '2', 'KL');

-- --------------------------------------------------------

--
-- Table structure for table `vm_salreturnitem`
--

CREATE TABLE `vm_salreturnitem` (
  `sri_billitemid` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `sri_billid` int(11) NOT NULL,
  `sri_returnbillid` int(11) NOT NULL,
  `sri_productid` int(11) NOT NULL,
  `sri_price` float NOT NULL,
  `sri_quantity` float NOT NULL,
  `sri_total` float NOT NULL,
  `sri_updatedon` datetime NOT NULL,
  `sri_isactive` int(11) NOT NULL,
  `sri_vatamount` float NOT NULL,
  `sri_vatper` float NOT NULL,
  `sri_unitprice` float NOT NULL,
  `sri_sgst` float DEFAULT NULL,
  `sri_sgstamt` float DEFAULT NULL,
  `sri_cgst` float DEFAULT NULL,
  `sri_cgstamt` float DEFAULT NULL,
  `sri_igst` float DEFAULT NULL,
  `sri_igstamt` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vm_salreturnitem`
--

INSERT INTO `vm_salreturnitem` (`sri_billitemid`, `user_id`, `sri_billid`, `sri_returnbillid`, `sri_productid`, `sri_price`, `sri_quantity`, `sri_total`, `sri_updatedon`, `sri_isactive`, `sri_vatamount`, `sri_vatper`, `sri_unitprice`, `sri_sgst`, `sri_sgstamt`, `sri_cgst`, `sri_cgstamt`, `sri_igst`, `sri_igstamt`) VALUES
(1, 1, 1, 1, 1, 190.48, 2, 400, '2017-07-01 17:21:57', 1, 19.05, 5, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 1, 1, 1, 2, 380.95, 2, 800, '2017-07-01 17:21:57', 1, 38.09, 5, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(3, 1, 2, 1, 1, 0, 1, 200, '2017-07-08 13:54:06', 0, 0, 0, 0, 0, 0, 0, 0, 5, 9.52),
(4, 1, 3, 2, 1, 0, 1, 200, '2017-07-08 14:05:05', 0, 0, 0, 0, 0, 0, 0, 0, 5, 9.52),
(5, 1, 4, 2, 1, 190.48, 1, 200, '2017-07-08 14:07:49', 0, 0, 0, 0, 0, 0, 0, 0, 5, 9.52);

-- --------------------------------------------------------

--
-- Table structure for table `vm_shopprofile`
--

CREATE TABLE `vm_shopprofile` (
  `sp_shopid` int(11) NOT NULL,
  `sp_shopname` varchar(600) DEFAULT NULL,
  `sp_shopaddress` varchar(1000) DEFAULT NULL,
  `sp_phone` varchar(20) DEFAULT NULL,
  `sp_mobile` varchar(20) DEFAULT NULL,
  `sp_email` varchar(350) DEFAULT NULL,
  `sp_logo` varchar(1000) DEFAULT NULL,
  `sp_username` varchar(50) DEFAULT NULL,
  `sp_password` varchar(50) DEFAULT NULL,
  `sp_isactive` int(11) DEFAULT '0',
  `sp_vatreadymades` float DEFAULT NULL,
  `sp_vatmillgoods` float DEFAULT NULL,
  `sp_tin` varchar(100) NOT NULL,
  `sp_cst` varchar(200) NOT NULL,
  `sp_adddate` date NOT NULL,
  `sp_acnttype` int(11) NOT NULL DEFAULT '0' COMMENT '(0)trail(1)payed',
  `sp_trlprd` int(11) NOT NULL,
  `sp_barcode` varchar(10) NOT NULL,
  `sp_stcode` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vm_shopprofile`
--

INSERT INTO `vm_shopprofile` (`sp_shopid`, `sp_shopname`, `sp_shopaddress`, `sp_phone`, `sp_mobile`, `sp_email`, `sp_logo`, `sp_username`, `sp_password`, `sp_isactive`, `sp_vatreadymades`, `sp_vatmillgoods`, `sp_tin`, `sp_cst`, `sp_adddate`, `sp_acnttype`, `sp_trlprd`, `sp_barcode`, `sp_stcode`) VALUES
(1, 'Selvam', 'Palakkad', '9876543210', '', '', NULL, 'admin', 'admin123', 0, 0, 0, '123456', '', '2017-06-23', 1, 1, 'US-ebiller', 'KL'),
(2, 'uesr1', '', '', '', '', NULL, 'new', '123', 0, NULL, NULL, '', '', '2017-07-07', 0, 10, 'user', 'KL');

-- --------------------------------------------------------

--
-- Table structure for table `vm_supplier`
--

CREATE TABLE `vm_supplier` (
  `rs_supplierid` int(11) NOT NULL,
  `rs_company_name` varchar(100) NOT NULL,
  `rs_name` varchar(50) DEFAULT NULL,
  `rs_phone` varchar(20) DEFAULT NULL,
  `rs_mobile` varchar(20) DEFAULT NULL,
  `rs_address` longtext,
  `rs_email` varchar(100) NOT NULL,
  `rs_balance` varchar(10) NOT NULL,
  `rs_isactive` int(11) DEFAULT '0',
  `rs_tinnum` varchar(100) NOT NULL,
  `user_id` varchar(10) NOT NULL,
  `rs_acntid` varchar(100) NOT NULL,
  `rs_statecode` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vm_supplier`
--

INSERT INTO `vm_supplier` (`rs_supplierid`, `rs_company_name`, `rs_name`, `rs_phone`, `rs_mobile`, `rs_address`, `rs_email`, `rs_balance`, `rs_isactive`, `rs_tinnum`, `user_id`, `rs_acntid`, `rs_statecode`) VALUES
(1, 'COMPANY', 'SUPPLIER', '0123456789', NULL, 'PALAKKAD', 'supplier@gmail.com', '-1000', 0, '963258741', '1', '14', 'KA'),
(2, 'company1', 'das', '9633919919', NULL, 'palakkad', '', '0', 0, '', '1', '31', NULL),
(3, 'sachin', 'sachin', '', NULL, '', '', '1000', 0, '', '1', '2', 'KL'),
(4, 'fg', 'cfgb', '', NULL, '', '', '', 0, '', '1', '', 'HP'),
(5, 'fdg', 'df', '', NULL, '', '', '', 0, '', '1', '', ''),
(6, 'ji', 'hj', '', NULL, '', '', '', 0, '', '1', '', 'KA');

-- --------------------------------------------------------

--
-- Table structure for table `vm_transaction`
--

CREATE TABLE `vm_transaction` (
  `tr_id` int(11) NOT NULL,
  `tr_billid` int(11) NOT NULL,
  `tr_particulars` varchar(500) NOT NULL,
  `tr_openingbalance` float NOT NULL,
  `tr_transactionamount` float NOT NULL,
  `tr_closingbalance` float NOT NULL,
  `tr_date` datetime NOT NULL,
  `tr_transactiontype` varchar(500) NOT NULL,
  `tr_isactive` int(11) NOT NULL DEFAULT '0',
  `tr_updateddate` datetime NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vm_transaction`
--

INSERT INTO `vm_transaction` (`tr_id`, `tr_billid`, `tr_particulars`, `tr_openingbalance`, `tr_transactionamount`, `tr_closingbalance`, `tr_date`, `tr_transactiontype`, `tr_isactive`, `tr_updateddate`, `user_id`) VALUES
(1, 1, 'Sales', 0, 2100, 2100, '2017-06-30 15:55:00', 'income', 0, '0000-00-00 00:00:00', 1),
(2, 2, 'Sales', 2100, 0, 2100, '2017-06-30 15:58:00', 'income', 0, '0000-00-00 00:00:00', 1),
(3, 3, 'Sales', 2100, 0, 2100, '2017-06-30 16:02:00', 'income', 0, '0000-00-00 00:00:00', 1),
(4, 4, 'Sales', 2100, 500, 2600, '2017-06-30 16:03:00', 'income', 0, '0000-00-00 00:00:00', 1),
(5, 1, 'Purchase', 2600, 5000, -2400, '2017-06-30 16:08:00', 'expense', 1, '0000-00-00 00:00:00', 1),
(6, 5, 'Sales Return', -2400, 1200, -3600, '2017-07-01 17:21:00', 'expense', 0, '0000-00-00 00:00:00', 1),
(7, 5, 'Sales', -3600, 0, -3600, '1970-01-01 05:30:00', 'income', 0, '0000-00-00 00:00:00', 1),
(8, 6, 'Sales', -3600, 200, -3400, '2017-07-05 03:30:00', 'income', 0, '0000-00-00 00:00:00', 1),
(9, 7, 'Sales', -3400, 200, -3200, '2017-07-05 03:31:00', 'income', 0, '0000-00-00 00:00:00', 1),
(10, 8, 'Sales', -3200, 534.25, -2665.75, '2017-07-05 03:37:00', 'income', 0, '0000-00-00 00:00:00', 1),
(11, 9, 'Sales', -2665.75, 200, -2465.75, '2017-07-05 05:22:00', 'income', 0, '0000-00-00 00:00:00', 1),
(12, 10, 'Sales', -2465.75, 100, -2365.75, '2017-07-05 05:25:00', 'income', 0, '0000-00-00 00:00:00', 1),
(13, 11, 'Sales', -2365.75, 200, -2165.75, '2017-07-06 09:59:00', 'income', 0, '0000-00-00 00:00:00', 1),
(14, 2, 'Purchase', -2165.75, 100, -2265.75, '2017-07-07 14:27:00', 'expense', 0, '0000-00-00 00:00:00', 1),
(15, 12, 'Sales', -2265.75, 200, -2065.75, '2017-07-07 14:38:00', 'income', 0, '0000-00-00 00:00:00', 1),
(16, 3, 'Purchase', -2065.75, 100, -2165.75, '2017-07-07 16:54:00', 'expense', 0, '0000-00-00 00:00:00', 1),
(17, 4, 'Purchase', -2165.75, 100, -2265.75, '2017-07-07 16:54:00', 'expense', 0, '0000-00-00 00:00:00', 1),
(18, 5, 'Purchase', -2265.75, 100, -2365.75, '2017-07-07 16:55:00', 'expense', 0, '0000-00-00 00:00:00', 1),
(19, 6, 'Purchase', -2365.75, 100, -2465.75, '2017-07-07 16:58:00', 'expense', 0, '0000-00-00 00:00:00', 1),
(20, 7, 'Purchase', -2465.75, 100, -2565.75, '2017-07-07 16:58:00', 'expense', 0, '0000-00-00 00:00:00', 1),
(21, 8, 'Purchase', -2565.75, 100, -2665.75, '2017-07-07 16:58:00', 'expense', 0, '0000-00-00 00:00:00', 1),
(22, 9, 'Purchase', -2665.75, 100, -2765.75, '2017-07-07 17:00:00', 'expense', 0, '0000-00-00 00:00:00', 1),
(23, 10, 'Purchase', -2765.75, 100, -2865.75, '2017-07-07 17:02:00', 'expense', 0, '0000-00-00 00:00:00', 1),
(24, 11, 'Purchase', -2865.75, 100, -2965.75, '2017-07-07 17:02:00', 'expense', 0, '0000-00-00 00:00:00', 1),
(25, 12, 'Purchase', -2965.75, 300, -3265.75, '2017-07-07 17:03:00', 'expense', 0, '0000-00-00 00:00:00', 1),
(26, 13, 'Purchase', -3265.75, 100, -3365.75, '2017-07-07 17:05:00', 'expense', 0, '0000-00-00 00:00:00', 1),
(27, 13, 'Sales', -3365.75, 925, -2440.75, '2017-07-07 17:49:00', 'income', 0, '0000-00-00 00:00:00', 1),
(28, 14, 'Sales', -2440.75, 200, -2240.75, '2017-07-07 17:49:00', 'income', 0, '0000-00-00 00:00:00', 1),
(29, 15, 'Sales', -2240.75, 189.5, -2051.25, '2017-07-07 18:08:00', 'income', 0, '0000-00-00 00:00:00', 1),
(30, 16, 'Sales', -2051.25, 200, -1851.25, '2017-07-07 18:09:00', 'income', 0, '0000-00-00 00:00:00', 1),
(31, 14, 'Purchase', -1851.25, 100, -1951.25, '2017-07-08 12:27:00', 'expense', 0, '0000-00-00 00:00:00', 1),
(32, 15, 'Purchase', -1951.25, 100, -2051.25, '2017-07-08 12:34:00', 'expense', 0, '0000-00-00 00:00:00', 1),
(33, 16, 'Purchase', -2051.25, 100, -2151.25, '2017-07-08 12:37:00', 'expense', 0, '0000-00-00 00:00:00', 1),
(34, 6, 'Sales Return', -2151.25, 200, -2351.25, '2017-07-08 13:53:00', 'expense', 0, '0000-00-00 00:00:00', 1),
(35, 7, 'Sales Return', -2351.25, 200, -2551.25, '2017-07-08 14:04:00', 'expense', 0, '0000-00-00 00:00:00', 1),
(36, 8, 'Sales Return', -2551.25, 200, -2751.25, '2017-07-08 14:07:00', 'expense', 0, '0000-00-00 00:00:00', 1),
(37, 1, 'Purchase Return', -2751.25, 100, -2651.25, '2017-07-08 15:38:00', 'income', 0, '0000-00-00 00:00:00', 1),
(38, 2, 'Purchase Return', -2651.25, 100, -2551.25, '2017-07-08 15:56:00', 'income', 0, '0000-00-00 00:00:00', 1),
(39, 3, 'Purchase Return', -2551.25, 100, -2451.25, '2017-07-08 16:03:00', 'income', 0, '0000-00-00 00:00:00', 1),
(40, 4, 'Purchase Return', -2451.25, 100, -2351.25, '2017-07-08 16:15:00', 'income', 0, '0000-00-00 00:00:00', 1),
(41, 5, 'Purchase Return', -2351.25, 100, -2251.25, '2017-07-08 16:15:00', 'income', 0, '0000-00-00 00:00:00', 1),
(42, 6, 'Purchase Return', -2251.25, 100, -2151.25, '2017-07-08 16:16:00', 'income', 0, '0000-00-00 00:00:00', 1),
(43, 7, 'Purchase Return', -2151.25, 300, -1851.25, '2017-07-08 16:18:00', 'income', 0, '0000-00-00 00:00:00', 1),
(44, 8, 'Purchase Return', -1851.25, 100, -1751.25, '2017-07-08 16:20:00', 'income', 0, '0000-00-00 00:00:00', 1),
(45, 9, 'Purchase Return', -1751.25, 100, -1651.25, '2017-07-08 16:44:00', 'income', 0, '0000-00-00 00:00:00', 1),
(46, 17, 'Sales', -1651.25, -50, -1701.25, '2017-07-10 15:28:00', 'income', 0, '0000-00-00 00:00:00', 1),
(47, 0, 'Sales', 0, 0, 0, '2017-07-10 16:33:16', 'income', 0, '0000-00-00 00:00:00', 1),
(48, 0, 'Sales', 0, 5, 0, '2017-07-10 16:49:31', 'income', 0, '0000-00-00 00:00:00', 1),
(49, 0, 'Sales', 0, 5, 0, '2017-07-10 16:51:20', 'income', 0, '0000-00-00 00:00:00', 1),
(50, 18, 'Sales', 0, 755, 755, '2017-07-11 14:31:00', 'income', 0, '0000-00-00 00:00:00', 1),
(51, 19, 'Sales', 755, 203.93, 958.93, '2017-07-11 14:38:00', 'income', 0, '0000-00-00 00:00:00', 1),
(52, 20, 'Sales', 958.93, 208.93, 1167.86, '2017-07-11 14:42:00', 'income', 0, '0000-00-00 00:00:00', 1),
(53, 21, 'Sales', 1167.86, 209.45, 1377.31, '2017-07-11 14:53:00', 'income', 0, '0000-00-00 00:00:00', 1),
(54, 22, 'Sales', 1377.31, 10526, 11903.3, '2017-07-11 15:30:00', 'income', 0, '0000-00-00 00:00:00', 1),
(55, 23, 'Sales', 11903.3, 200, 12103.3, '2017-07-13 17:34:00', 'income', 0, '0000-00-00 00:00:00', 1),
(56, 24, 'Sales', 12103.3, 400, 12503.3, '2017-07-13 17:35:00', 'income', 0, '0000-00-00 00:00:00', 1),
(57, 25, 'Sales', 12503.3, 1350, 13853.3, '2017-07-13 19:41:00', 'income', 0, '0000-00-00 00:00:00', 1),
(58, 26, 'Sales', 13853.3, 1800, 15653.3, '2017-07-15 11:26:00', 'income', 0, '0000-00-00 00:00:00', 1),
(59, 27, 'Sales', 15653.3, 1967, 17620.3, '2017-07-15 15:25:00', 'income', 0, '0000-00-00 00:00:00', 1),
(60, 28, 'Sales', 17620.3, 19000, 36620.3, '2017-07-15 15:29:00', 'income', 0, '0000-00-00 00:00:00', 1),
(61, 29, 'Sales', 36620.3, 200, 36820.3, '2017-07-17 18:22:00', 'income', 0, '0000-00-00 00:00:00', 1),
(62, 10, 'Purchase Return', 36820.3, 100, 36920.3, '2017-07-18 16:40:00', 'income', 0, '0000-00-00 00:00:00', 1),
(63, 17, 'Purchase', 36920.3, 3000, 33920.3, '2017-07-18 17:42:00', 'expense', 0, '0000-00-00 00:00:00', 1),
(64, 31, 'Sales', 33920.3, 368, 34288.3, '2017-07-24 08:51:00', 'income', 0, '0000-00-00 00:00:00', 1),
(65, 32, 'Sales', 34288.3, 268, 34556.3, '2017-07-24 08:53:00', 'income', 0, '0000-00-00 00:00:00', 1),
(66, 33, 'Sales', 34556.3, 315, 34871.3, '2017-07-24 08:54:00', 'income', 0, '0000-00-00 00:00:00', 1),
(67, 34, 'Sales', 34871.3, 336, 35207.3, '2017-07-24 09:27:00', 'income', 0, '0000-00-00 00:00:00', 1),
(68, 35, 'Sales', 35207.3, 410, 35617.3, '2017-07-24 09:28:00', 'income', 0, '0000-00-00 00:00:00', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `administrator_account_name`
--
ALTER TABLE `administrator_account_name`
  ADD PRIMARY KEY (`refid`);

--
-- Indexes for table `administrator_daybook`
--
ALTER TABLE `administrator_daybook`
  ADD PRIMARY KEY (`refid`);

--
-- Indexes for table `vm_billentry`
--
ALTER TABLE `vm_billentry`
  ADD PRIMARY KEY (`be_billid`);

--
-- Indexes for table `vm_billitems`
--
ALTER TABLE `vm_billitems`
  ADD PRIMARY KEY (`bi_billitemid`);

--
-- Indexes for table `vm_catogory`
--
ALTER TABLE `vm_catogory`
  ADD PRIMARY KEY (`ca_categoryid`);

--
-- Indexes for table `vm_customer`
--
ALTER TABLE `vm_customer`
  ADD PRIMARY KEY (`cs_customerid`);

--
-- Indexes for table `vm_customeritemprice`
--
ALTER TABLE `vm_customeritemprice`
  ADD PRIMARY KEY (`cp_id`);

--
-- Indexes for table `vm_estimation`
--
ALTER TABLE `vm_estimation`
  ADD PRIMARY KEY (`be_billid`);

--
-- Indexes for table `vm_estimationitems`
--
ALTER TABLE `vm_estimationitems`
  ADD PRIMARY KEY (`bi_billitemid`);

--
-- Indexes for table `vm_payment`
--
ALTER TABLE `vm_payment`
  ADD PRIMARY KEY (`pa_paymentid`);

--
-- Indexes for table `vm_products`
--
ALTER TABLE `vm_products`
  ADD PRIMARY KEY (`pr_productid`);

--
-- Indexes for table `vm_purentry`
--
ALTER TABLE `vm_purentry`
  ADD PRIMARY KEY (`pe_billid`);

--
-- Indexes for table `vm_puritems`
--
ALTER TABLE `vm_puritems`
  ADD PRIMARY KEY (`pi_billitemid`);

--
-- Indexes for table `vm_purorder`
--
ALTER TABLE `vm_purorder`
  ADD PRIMARY KEY (`pe_billid`);

--
-- Indexes for table `vm_purorderitems`
--
ALTER TABLE `vm_purorderitems`
  ADD PRIMARY KEY (`pi_billitemid`);

--
-- Indexes for table `vm_purreturnentry`
--
ALTER TABLE `vm_purreturnentry`
  ADD PRIMARY KEY (`pre_billid`);

--
-- Indexes for table `vm_purreturnitem`
--
ALTER TABLE `vm_purreturnitem`
  ADD PRIMARY KEY (`pri_billitemid`);

--
-- Indexes for table `vm_salreturnentry`
--
ALTER TABLE `vm_salreturnentry`
  ADD PRIMARY KEY (`sre_billid`);

--
-- Indexes for table `vm_salreturnitem`
--
ALTER TABLE `vm_salreturnitem`
  ADD PRIMARY KEY (`sri_billitemid`);

--
-- Indexes for table `vm_shopprofile`
--
ALTER TABLE `vm_shopprofile`
  ADD PRIMARY KEY (`sp_shopid`);

--
-- Indexes for table `vm_supplier`
--
ALTER TABLE `vm_supplier`
  ADD PRIMARY KEY (`rs_supplierid`);

--
-- Indexes for table `vm_transaction`
--
ALTER TABLE `vm_transaction`
  ADD PRIMARY KEY (`tr_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `administrator_account_name`
--
ALTER TABLE `administrator_account_name`
  MODIFY `refid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `administrator_daybook`
--
ALTER TABLE `administrator_daybook`
  MODIFY `refid` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `vm_billentry`
--
ALTER TABLE `vm_billentry`
  MODIFY `be_billid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `vm_billitems`
--
ALTER TABLE `vm_billitems`
  MODIFY `bi_billitemid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;
--
-- AUTO_INCREMENT for table `vm_catogory`
--
ALTER TABLE `vm_catogory`
  MODIFY `ca_categoryid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `vm_customer`
--
ALTER TABLE `vm_customer`
  MODIFY `cs_customerid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `vm_customeritemprice`
--
ALTER TABLE `vm_customeritemprice`
  MODIFY `cp_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `vm_estimation`
--
ALTER TABLE `vm_estimation`
  MODIFY `be_billid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `vm_estimationitems`
--
ALTER TABLE `vm_estimationitems`
  MODIFY `bi_billitemid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `vm_payment`
--
ALTER TABLE `vm_payment`
  MODIFY `pa_paymentid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `vm_products`
--
ALTER TABLE `vm_products`
  MODIFY `pr_productid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;
--
-- AUTO_INCREMENT for table `vm_purentry`
--
ALTER TABLE `vm_purentry`
  MODIFY `pe_billid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `vm_puritems`
--
ALTER TABLE `vm_puritems`
  MODIFY `pi_billitemid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `vm_purorder`
--
ALTER TABLE `vm_purorder`
  MODIFY `pe_billid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `vm_purorderitems`
--
ALTER TABLE `vm_purorderitems`
  MODIFY `pi_billitemid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `vm_purreturnentry`
--
ALTER TABLE `vm_purreturnentry`
  MODIFY `pre_billid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `vm_purreturnitem`
--
ALTER TABLE `vm_purreturnitem`
  MODIFY `pri_billitemid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `vm_salreturnentry`
--
ALTER TABLE `vm_salreturnentry`
  MODIFY `sre_billid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `vm_salreturnitem`
--
ALTER TABLE `vm_salreturnitem`
  MODIFY `sri_billitemid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `vm_shopprofile`
--
ALTER TABLE `vm_shopprofile`
  MODIFY `sp_shopid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `vm_supplier`
--
ALTER TABLE `vm_supplier`
  MODIFY `rs_supplierid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `vm_transaction`
--
ALTER TABLE `vm_transaction`
  MODIFY `tr_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
