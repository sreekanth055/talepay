 <footer id="footer">
    <p class="no-s">Eazy Pay Biller powered by <a href="http://canrone.com/" target="_blank">Canrone Software Solutions Pvt. Ltd.</a></p>
</footer>

  <!-- plugins:js -->
  <script src="../assets/js/vendor.bundle.base.js"></script>
  <!-- endinject -->
  <!-- inject:js -->
  <script src="../assets/js/off-canvas.js"></script>
  <script src="../assets/js/hoverable-collapse.js"></script>
  <script src="../assets/js/misc.js"></script>
  <script src="../assets/js/settings.js"></script>
  <script src="../assets/js/todolist.js"></script>
  <!-- endinject -->
  <!-- Custom js for this page-->
  <!-- End custom js for this page-->
</body>

</html>


